/*
 * $Id: rk_vars.c,v 2.3.2.1 2000/08/04 05:37:30 kaneda Exp $
 */
/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/
/***********************************************************************
			rk_vars.c
						88. 6.16  訂 補
						93.10.19 

	プログラム内で二つ以上のファイルにまたがって使う変数を
	まとめて定義している。
***********************************************************************/
/*	Version 3.0
 */
#include "rk_header.h"
#include "rk_extvars.h"


char	rk_errstat = 0; /* (error)によってエラーが引き起こされた時1（但し
			   (eofflg)が立っていたため末尾までの強制出力処理が
			   起こった場合は2）になる。romkan_henkan()実行ごとに
			   更新される。これはユーザにも開放。*/

int	flags = 0;
 /* 以下のフラグのOR。
	RK_CHMOUT モードチェンジを知らせるかのフラグ 
	RK_KEYACK キーインに対し必ず何か返すかのフラグ 
	RK_DSPNIL romkan_disp(off)modeの返値のデフォルトが空文字列であるかの
		  フラグ。立たなければデフォルトはNULL。互換性のためromkan_
		  init2まではこれが立つ。
	RK_NONISE 偽コードを出さないようにするかのフラグ
	RK_REDRAW Wnnで使う特殊なフラグ。これが立っている場合、romkan_henkan
		  の結果としてdisoutにnisedl（偽物のdel）以後が特殊コードのみ
		  で終わる文字列が返ってきたら、その後ろにREDRAWをつなぐ。Wnn
		  で変換行のredrawをするために必要な措置。
	RK_SIMPLD deleteの動作を単純にするかのフラグ
	RK_VERBOS verboseで起こすかのフラグ
 */

jmp_buf env0;

FILE	*modefile; /* モード定義表のファイル */
char	nulstr[1] = {'\0'};

char	*hcurread, *mcurread; /* 変換表・モード表の現在行bufへのポインタ */
char	*curfnm, *curdir; /* 現在読んでる変換表・モード表の名とディレクトリ。
     但しcurdirは、サーチパスを捜してかつ表のオープンに成功した時のみ有効 */
letter	*ltrbufbgn; /* 使用中のletterのバッファの先頭を指す */
  /* これら五つは、readdata()などの関数内でローカルに定義する配列の先頭を指
     す。従って、その関数を抜けた後でこれらの値を使わないよう十分注意。特に、
     エラー警告ルーチンなどでは、longjmpで戻るより前にこれらのprintを行うこと
     （エラー処理用に使っているのは、上から四個まで）。*/
FILE	*nestfile[FILNST], **base; /* 変換対応表のファイル */

struct	modestat modesw[MODMAX];

char	hyoshu[HYOMAX]; /* 表の種別（前・本・後処理表の区別）を入れる配列 */
char	**modmeiptr, *modmeibgn[MODMAX]; /* モード名に対する上と同様のデータ*/
char	*modmeimem, modmeimem_[MODMEI]; /*		〃		*/
char	**dspnamptr, *dspnambgn[DMDMAX];
				/* モード表示文字列に対する同様のデータ */
char	*dspcod, dspcod_[DMDCHR]; /*		〃		*/
char	*dspmod[2][2] = {{NULL, NULL}, {NULL, NULL}};
	 /* 現在及び一つ前の、romkanをそれぞれon・offしている時のモード表示
	    文字列へのポインタ。romkan_dispmode()で返る値はdspmod[0][0]。*/

char	**hyomeiorg, **hyomeiptr;
	 /* 表の名へのポインタを入れる配列 *hyomeibgn[HYOMAX] を readdata() で
	    使うが、その先頭及び特定の要素へのポインタ */
char	*hyomeimem;
	 /* 表の名の実際の文字列を入れる配列 hyomeimem_[HYOMEI] を readdata()
	    で使うが、その特定の要素へのポインタ */
char	**pathmeiorg, **pathmeiptr;
	 /* 対応表のサーチパス名へのポインタを入れる配列 *pathmeibgn[PTHMAX]を
	    readdata() で使うが、その先頭及び特定の要素へのポインタ */
char	*pathareaorg, *pathmeimem;
	 /* 上記サーチパス名の実際の文字列を入れる配列 pathmeimem_[PTHMEI] を
	    readdata() で使うが、その先頭及び特定の要素へのポインタ。
	    但しpathmeimem_の先頭にはモード表のあるディレクトリの名が入り、後
	    で使うので、pathareaorgにはその続きのエリアの先頭の番地が入る。*/

int	usemaehyo[HYOMAX], usehyo[HYOMAX], useatohyo[HYOMAX];
	 /* 前・本・後処理表のどれが選択されているかのデータ */
int	naibu_[NAIBMX], *naibu; /* モード表の内部表現を入れる配列 */

letter	*lptr; /* letterへの汎用ポインタ */

letter	rk_input; /* 入力。3バイトまでは変更なしに対応可 */
letter	disout[OUTSIZ]; /* 本処理からの出力のバッファ */
letter	rk_output[OUTSIZ]; /* 後処理からの出力（最終出力）のバッファ */
letter	keybuf[KBFSIZ], urabuf[KBFSIZ]; /* 本処理バッファとその退避領域 */
int	lastoutlen, lastkbflen;
letter	oneletter[2] = {EOLTTR, EOLTTR}, nil[1] = {EOLTTR};
int	hyonum;

struct	hyo	hyo_n[HYOMAX]; /* 表ごとの変域・対応データの開始番地 */


#define bit3(x, y, z) ((x) | ((y) << 1) | ((z) << 2))

struct	funstr	func[] = /* 関数の追加にはここと mchevl(), evlis() をいじる */
{
/*			引数の数（-1…この値未使用）
	  名		    出現     ｜	 タイプ…0:文字関数 1:文字列関数
	  ↓		     ↓	     ↓	 ↓	 2:特殊関数 3:機能 4:宣言 */
	{ "defvar",	bit3(1,0,0), -1, 4 }, /*	0 */
	{ "include",	bit3(1,0,0), -1, 4 },
	{ "toupper",	bit3(1,1,1),  1, 0 },
	{ "tolower",	bit3(1,1,1),  1, 0 },
	{ "off",	bit3(0,1,0), -1, 2 },
	{ "on",		bit3(0,1,0), -1, 2 }, /*	5 */
	{ "switch",	bit3(0,1,0), -1, 2 },
	{ "toupdown",	bit3(1,1,1),  1, 0 },
	{ "tozenalpha",	bit3(1,1,1),  1, 0 },
	{ "tohira",	bit3(1,1,1),  1, 0 },
	{ "tokata",	bit3(1,1,1),  1, 0 }, /* 10 */
	{ "tohankata",	bit3(1,1,1),  1, 1 },
	{ "tozenhira",	bit3(1,1,1),  1, 0 },
	{ "tozenkata",	bit3(1,1,1),  1, 0 },
	{ "+",		bit3(1,1,1),  2, 0 },
	{ "-",		bit3(1,1,1),  2, 0 }, /* 15 */
	{ "*",		bit3(1,1,1),  2, 0 },
	{ "/",		bit3(1,1,1),  2, 0 },
	{ "%",		bit3(1,1,1),  2, 0 },
	{ "last=",	bit3(1,0,0),  1, 2 },  /* 直前のマッチコード==引数か? */
	{ "if",		bit3(1,0,0), -1, 2 }, /* 20 */
	{ "unless",	bit3(1,0,0), -1, 2 },
	{ "restart",	bit3(0,1,0),  0, 3 },
	{ "delchr",	bit3(1,1,1),  0, 0 },  /* delchrを返す。隠しコマンド */
	{ "alloff",	bit3(0,1,0),  0, 2 },  /* 全モードをいっぺんにOFF */
	{ "allon",	bit3(0,1,0),  0, 2 }, /* 25 */
	{ "bitand",	bit3(1,1,1),  2, 0 },
	{ "bitor",	bit3(1,1,1),  2, 0 },
	{ "bitnot",	bit3(1,1,1),  1, 0 },
	{ "!",		bit3(0,1,0),  0, 2 },  /* 裏バッファの明示的クリア */
	{ "atEOF",	bit3(1,0,0),  0, 2 }, /* 30 */
	{ "todigit",	bit3(1,1,1),  2, 1 },
	{ "dakuadd",	bit3(1,1,1),  1, 1 },  /* 濁点の付加 */
	{ "handakuadd",	bit3(1,1,1),  1, 1 },  /* 半濁点の付加 */
	{ "value",	bit3(1,1,1),  1, 0 },
	{ "error",	bit3(0,1,0),  0, 3 }, /* 35 */
	{ "defconst",	bit3(1,0,0), -1, 4 },
	{ "setmode",	bit3(0,1,0), -1, 2 },
	{ "mode+",	bit3(0,1,0), -1, 2 },
	{ "mode-",	bit3(0,1,0), -1, 2 },
	{ "mode=",	bit3(1,0,0), -1, 2 }, /* 40 */
	{ "mode!=",	bit3(1,0,0), -1, 2 },
	{ "mode<",	bit3(1,0,0), -1, 2 },
	{ "mode>",	bit3(1,0,0), -1, 2 },
	{ "send",	bit3(0,1,0),  1, 1 },  /* 試作 */
	{ NULL,		0,            0, 0 }
};
	/* last=〜unless… 入力コードの位置にしか置けず、文字列関数扱いなので
			本処理表にしか書けない。*/
	/* +,-,*,/,bitand,bitor,bitnot… 3バイト分の演算を行う。*/
	/* atEOF… ファイル末尾での特別動作を指定するためのもの。但し、その
		動作は不完全。*/

