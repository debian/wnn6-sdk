/*
 * $Id: rk_bltinfn.c,v 2.7.2.1 2000/08/04 05:37:26 kaneda Exp $
 */
/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/
/***********************************************************************
			rk_bltinfn.c
						87.12. 6  訂 補
						93.10.19

	変換用の組み込み関数のうち複雑なものを定義してある。
	全角←→半角の変換が主体。
***********************************************************************/
/*	Version 3.0
 */
#include "commonhd.h"
#include "config.h"

#include "rk_multi.h"

 /* 半角文字のコードのdefine */
#define HKCHOU	(HNKAK1 * 0x100 + 0xB0) /* ｰ */
#define HKDKTN	(HNKAK1 * 0x100 + 0xDE) /* ﾞ */
#define HKHNDK	(HNKAK1 * 0x100 + 0xDF) /* ﾟ */
#define HKMARU	(HNKAK1 * 0x100 + 0xA1) /* ｡ */
#define HKHRKG	(HNKAK1 * 0x100 + 0xA2) /* ｢ */
#define HKTJKG	(HNKAK1 * 0x100 + 0xA3) /* ｣ */
#define HKTTEN	(HNKAK1 * 0x100 + 0xA4) /* ､ */
#define HKNKPT	(HNKAK1 * 0x100 + 0xA5) /* ･ */

 /* 全角文字のコードのdefine */
#define CHOUON	(0xA1BC) /* ー */
#define DAKUTN	(0xA1AB) /* ゛ */
#define HNDAKU	(0xA1AC) /* ゜ */
#define MNMARU	(0xA1A3) /* 。 */ /* 名前は MaNMARU（まんまる）の略 */
#define HRKKAG	(0xA1D6) /* 「 */
#define TJIKAG	(0xA1D7) /* 」 */
#define TOUTEN	(0xA1A2) /* 、 */
#define NKPOTU	(0xA1A6) /* ・ */

static char _lang[6];

void
romkan_set_lang_body(lang, cur_rk)
char *lang;
ARGS *cur_rk;
{
	strncpy(_lang, lang, 5);
	_lang[5] = 0;
}

void
romkan_set_lang(lang)
char *lang;
{
    ARGS *cur_rk = NULL;

    LockMutex(&(cur_rk->rk_lock));
    romkan_set_lang_body(lang, cur_rk);
    UnlockMutex(&(cur_rk->rk_lock));
}

 /** ASCII文字→全角 */
letter
to_zenalpha(l, cur_rk)
letter	l;
ARGS *cur_rk;
{
	letter	retval;

	static	uns_chr *data = (uns_chr *)
	   "　！”＃＄％＆’（）＊＋，−．／０１２３４５６７８９：；＜＝＞？\
＠ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ［￥］＾＿｀ａｂｃｄｅ\
ｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ｛｜｝￣";

#ifdef	CHINESE
	static	uns_chr *data_cn = (uns_chr *)
	   "　�　隠�∞�ィΓВ┌�☆����！����０１２３４５６７８９�今察苅宗毅�\
�寸腺贈達庁釘藤韮硲稗複烹味唯裡錬丕傳劭咤圍妝孱廝悖截擅曖ぃ檻泯滷爍瓧癸磽筍�\
ｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ��������";
#endif	/* CHINESE */
#ifdef	KOREAN
	static  uns_chr *data_ko = (uns_chr *)
	   "　�。■��ぃィΓВ┌�������������０１２３４５６７８９�今撮治州升�\
�寸腺贈達庁釘藤韮硲稗複烹味唯裡錬丕傳劭咤圍妝孱廝悖截擅曖棕檻泯滷爍瓧癸磽筍�\
ｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ��������";
#endif  /* KOREAN */

	if(' ' <= l && l <= '~') {
		l = (l - ' ') << 1;
#ifdef	CHINESE
	    if(!strcmp(_lang, WNN_C_LANG) || !strcmp(_lang, WNN_T_LANG)){
		retval = data_cn[l] << 8;
		retval += data_cn[++l];
	    } else
#endif
#ifdef  KOREAN
	    if(!strcmp(_lang, WNN_K_LANG)){
		retval = data_ko[l] << 8;
		retval += data_ko[++l];
	    } else
#endif  /* KOREAN */
	    {
		retval = data[l] << 8;
		retval += data[++l];
	    }
		return(retval);
	} else return(l);
}

static	char	*hankdata[] = {
	"ｧ","ｱ","ｨ","ｲ","ｩ","ｳ","ｪ","ｴ","ｫ","ｵ",
	"ｶ","ｶﾞ","ｷ","ｷﾞ","ｸ","ｸﾞ","ｹ","ｹﾞ","ｺ","ｺﾞ",
	"ｻ","ｻﾞ","ｼ","ｼﾞ","ｽ","ｽﾞ","ｾ","ｾﾞ","ｿ","ｿﾞ",
	"ﾀ","ﾀﾞ","ﾁ","ﾁﾞ","ｯ","ﾂ","ﾂﾞ","ﾃ","ﾃﾞ","ﾄ","ﾄﾞ",
	"ﾅ","ﾆ","ﾇ","ﾈ","ﾉ",
	"ﾊ","ﾊﾞ","ﾊﾟ","ﾋ","ﾋﾞ","ﾋﾟ","ﾌ","ﾌﾞ","ﾌﾟ",
	"ﾍ","ﾍﾞ","ﾍﾟ","ﾎ","ﾎﾞ","ﾎﾟ",
	"ﾏ","ﾐ","ﾑ","ﾒ","ﾓ",
	"ｬ","ﾔ","ｭ","ﾕ","ｮ","ﾖ",
	"ﾗ","ﾘ","ﾙ","ﾚ","ﾛ",
	"ヮ","ﾜ","ヰ","ヱ","ｦ","ﾝ",
	"ｳﾞ","ヵ","ヶ"
};  /* 全角が混じってるので注意 */

 /**	上のhankdataが、実際に使う半角コードを表していないとき、実際のものに
	修正する。初期設定時に一回だけ呼ぶ */
void
hank_setup()
{
	int	i;
	char	*s, orig_hnkak1;

	orig_hnkak1 = *hankdata[0];
 /*	*hankdata[] での半角文字の１バイトめ。半角文字の１バイトめだけが異なる
	ような他機種に移植するときは、HNKAK1のdefineを変えればOK。但しromkanの
	ソース中の半角文字（このファイルにのみ存在）もコンバートして、その機種
	に合わせるほうが望ましい。しかし、エディタでこのファイルを修正したり
	する場合に、半角文字の扱いがうまくいかない場合があるので、特に
	コンバートをしなくとも動作するように処置はしてある。それが、この
	hank_setup()である。hankdataは、初期設定時に hank_setup()によって
	実際の半角コードに直される。*/

	if(orig_hnkak1 == (char)HNKAK1) return;
	for(i = 0; i < numberof(hankdata); i++){
		for(s = hankdata[i]; *s; s += 2)
			if(*s == orig_hnkak1) *s = HNKAK1;
	}
}

 /** かな→半角カタカナ。結果が二文字になることもある。*/
void
to_hankata(in, outp)
letter	in, **outp;
{
	uns_chr *p, c;
	letter	*out;
	int n;

	out = *outp;
	switch(in){
		case CHOUON: *out++ = HKCHOU; break;
		case DAKUTN: *out++ = HKDKTN; break;
		case HNDAKU: *out++ = HKHNDK; break;
		case MNMARU: *out++ = HKMARU; break;
		case HRKKAG: *out++ = HKHRKG; break;
		case TJIKAG: *out++ = HKTJKG; break;
		case TOUTEN: *out++ = HKTTEN; break;
		case NKPOTU: *out++ = HKNKPT; break;
		default:
			if(is_kata(in)){
				for(p = (uns_chr *)hankdata[in - KATBGN];
				    (c = *p); p++)
					*out++ = (c << 8) + *++p;
			} else 
			if(is_hira(in)){
				for(p = (uns_chr *)hankdata[in - HIRBGN];
				    (c = *p); p++)
					*out++ = (c << 8) + *++p;
			} else {
				*out++ = in;
			}
	}
	*out = EOLTTR;
	*outp = out;
}

 /**	半角カタカナ→ひらがな。但し、濁点を持つ文字を一つにまとめては
	くれないので注意。*/
letter
to_zenhira(l, cur_rk)
letter	l;
ARGS *cur_rk;
{
	letter	retval;

	static	uns_chr *data = (uns_chr *)
	   "。「」、・をぁぃぅぇぉゃゅょっーあいうえおかきくけこさしすせそた\
ちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわん゛゜";

#ifdef	CHINESE
	static	uns_chr *data_cn = (uns_chr *)
	   "。仝々、，をぁぃぅぇぉゃゅょっーあいうえお宅きくけこ業しすせそた\
ちつてとなにぬね議はひふへほまみむめ匆やゆよらりるれろわん≦＜";
#endif	/* CHINESE */

	if(is_hankata(l)){
		l = (l - HKKBGN) << 1;
#ifdef	CHINESE
	    if(!strcmp(_lang, WNN_C_LANG) || !strcmp(_lang, WNN_T_LANG)){
		retval = data_cn[l] << 8;
		retval += data_cn[++l];
	    } else
#endif
	    {
		retval = data[l] << 8;
		retval += data[++l];
	    }
		return(retval);
	} else return(l);
}

 /**	半角カタカナ→全角。但し、濁点を持つ文字を一つにまとめては
	くれないので注意。*/
letter
to_zenkata(l, cur_rk)
letter	l;
ARGS *cur_rk;
{
	return(is_hankata(l) ? (l = to_zenhira(l, cur_rk), to_kata(l)) : l);
}

 /* ビットベクタの構成 */
#define bitvec(b0, b1, b2, b3, b4, b5, b6, b7) (			 \
	(char)b0 | ((char)b1 << 1) | ((char)b2 << 2) | ((char)b3 << 3) | ((char)b4 << 4) | ((char)b5 << 5) | \
	((char)b6 << 6) | ((char)b7 << 7)						 \
)

 /** charの配列 h をビットベクタと見てその第iビットをチェックする */
#define bitlook(h, i) (h[(i) >> 3] & (1 << ((i) & 7)))

#define KATRPT	0xA1B3 /* ヽ */
#define HIRRPT	0xA1B5 /* ゝ */
#define KATA_U	0xA5A6 /* ウ */
#define KAT_VU	0xA5F4 /* ヴ */
#define HIR_KA	0xA4AB /* か */
#define HIR_HO	0xA4DB /* ほ */
#define KAT_KA	0xA5AB /* カ */
#define KAT_HO	0xA5DB /* ホ */
#define HIR_HA	0xA4CF /* は */
#define KAT_HA	0xA5CF /* ハ */

 /**	後ろに半濁点をくっつける。結果は一又は二文字。*/
void
handakuadd(in, outp)
letter	in, **outp;
{
	if((HIR_HA <= in && in <= HIR_HO) ? 0 == (in - HIR_HA) % 3 :
	   (KAT_HA <= in && in <= KAT_HO && 0 == (in - KAT_HA) % 3)){
		*(*outp)++ = in + 2;
	} else {
		*(*outp)++ = in;
		*(*outp)++ = HNDAKU;
	}
	**outp = EOLTTR;
}

 /**	後ろに濁点をくっつける。結果は一又は二文字。*/
void
dakuadd(in, outp)
letter	in, **outp;
{
	static	char	flgbit[] = {
		bitvec(1, 0, 1, 0, 1, 0, 1, 0), /* かがきぎくぐけげ */
		bitvec(1, 0, 1, 0, 1, 0, 1, 0), /* こごさざしじすず */
		bitvec(1, 0, 1, 0, 1, 0, 1, 0), /* せぜそぞただちぢ */
		bitvec(0, 1, 0, 1, 0, 1, 0, 0), /* っつづてでとどな */
		bitvec(0, 0, 0, 0, 1, 0, 0, 1), /* にぬねのはばぱひ */
		bitvec(0, 0, 1, 0, 0, 1, 0, 0), /* びぴふぶぷへべぺ */
		bitvec(1, 0, 0, 0, 0, 0, 0, 0)	/* ほ */
	};
	letter	c;

	if((HIR_KA <= in && in <= HIR_HO) ? (c = in - HIR_KA, 1) :
	   (KAT_KA <= in && in <= KAT_HO && (c = in - KAT_KA, 1))){
		if(bitlook(flgbit, c)){
			*(*outp)++ = in + 1;
		} else {
			*(*outp)++ = in;
			*(*outp)++ = DAKUTN;
		}
	} else switch(in){
		case KATRPT:
		case HIRRPT:
			*(*outp)++ = in + 1; break;
		case KATA_U:
			*(*outp)++ = KAT_VU; break;
		default:
			*(*outp)++ = in;
			*(*outp)++ = DAKUTN;
	}
	**outp = EOLTTR;
}

 /** inで与えられたコードをbase進の数字にしてoutpに入れる。*/
void
to_digit(in, lbase, outp)
letter	in, lbase, **outp;
{
	letter	c, vtol();

	if(c = in, c /= lbase) to_digit(c, lbase, outp);
	*(*outp)++ = vtol(in % lbase);
	**outp = EOLTTR;
}
