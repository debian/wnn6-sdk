/*
 * $Id: rk_header.h,v 2.3.2.1 2000/08/04 05:37:27 kaneda Exp $
 */
/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/
/***********************************************************************
			rk_header.h
						88. 5.20  訂 補

	rk_main.c rk_read.c rk_modread.c rk_bltinfn.cの共通ヘッダ。
	中で取っている配列の大きさなどを定義。
***********************************************************************/
/*	Version 3.0
 */
/*	make時に必要なdefine

	BSD42		BSDにてstrings.hを使用（string.hを使う場合は不要）
	SYSVR2		System Vにて定義域の制限されたtoupper・tolowerを使用
			（なくても動く）
	MVUX		ECLIPSE MVでの運転時にdefine  IKISが自動defineされる

	RKMODPATH="文字列"
			その文字列をモード定義表のサーチパスの環境変数の
			名前にする
	WNNDEFAULT	「@LIBDIR」で標準設定表のあるディレクトリを表せる
			ようにする	
	IKIS		半角仮名の１バイト目を0xA8（デフォルトは0x8E）にする

	この他 デバッグ時は必要に応じて KDSP、CHMDSPをdefine
*/

#ifdef MVUX
#  define IKIS
#endif

#include <stdio.h>
#ifdef BSD42
#  include <strings.h>
#  define strchr  index
#  define strrchr rindex
#else
#  include <string.h>
#endif
#include "rk_macros.h"

#define ESCCHR	'\033'
#define BASEMX	(26 + 10)

#define REALFN	200 /* 表のフルネームの最大長 */

#ifdef KOREAN
#define LINALL	30000 /* 対応表全部の行数合計 */
#define SIZALL	300000 /* 対応表の内部表現の最大サイズ*/
#else
#define LINALL	2000 /* 対応表全部の行数合計 */
#define SIZALL	20000 /* 対応表の内部表現の最大サイズ・
			表一つの変数の変域長の合計としても使ってる */
#endif

#define LINSIZ	1000 /* 対応表の一行の最大サイズ */
#define TRMSIZ	500 /* 対応表の一項目の最大サイズ・
			モード名の最長としても使ってる */
#define KBFSIZ	100 /* 本処理バッファのサイズ */
#define DSPLIN	256 /* デバッグ用 */
#define OUTSIZ	200 /* 出力バッファのサイズ */
#define RSLMAX	20 /* 関数の値として返る文字列の最長 */

#define VARMAX	50 /* 表一個の変数個数 */
#define VARTOT	2000 /* 全表の変数個数計 */
#define VARLEN	500 /* 変数名の長さの計 */

#define FILNST	20

	/* rk_modread.cで使うdefine */

#define HYOMAX	40 /* 変換対応表の最大個数 */
#define HYOMEI	500 /* 表名の文字数計 */
#define PTHMAX	30 /* サーチパスの最大個数 */
#define PTHMEI	800 /* サーチパス名の文字数計 */
#define MODMAX	50 /* モードの種類数 */
#define MODMEI	300 /* モードの全文字数 */
#define DMDMAX	40 /* モード表示の種類数 */
#define DMDCHR	250 /* モード表示の全文字数 */
#define MDHMAX	2500 /* モード表の最大サイズ */
  /* モード表の最初のlistscanの時は、エラーを考慮して、リスト1個のbufferに
	表のサイズ分取っておく。*/
#define MDT1LN	200 /* モード設定リスト1個の最大長 */
#define NAIBMX	400 /* モード定義表の内部表現の最大サイズ */
			/* Change KURI 200 --> 400 */

#define ARGMAX	2 /* 条件判断関数の引数の最大個数 */

 /* ディレクトリ名の区切りのdefine（UNIX用）。UNIX以外の環境で使うには
    これと、fixednamep()も変更の必要がある（readmode()のgetenv関係も勿論）。*/
#define KUGIRI '/'

 /* エラー処理用 */
#ifndef _WNN_SETJMP
#define _WNN_SETJMP
#include <setjmp.h>
#endif
