/*
 * $Id: wnnerrmsg.c,v 2.11.2.1 2000/08/04 05:37:24 kaneda Exp $
 */

/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/

/*	Version 4.0
 */
#include <stdio.h>
#include "jd_sock.h"
#include "jslib.h"
#include "jllib.h"
#include "commonhd.h"
#include "wnn_os.h"
#include "msg.h"

#include "mt_jlib.h"
#include "mt_jserver.h"

#ifdef	not_use
char *wnn_errormsg[] ={
"no_error 0",
"ファイルが存在しません。",		/*WNN_NO_EXIST	1 */
"no_error 2",
"メモリを確保できません。",		/*WNN_MALLOC_ERR	3 */
"no_error 4",
"辞書ではありません。",		/*WNN_NOT_A_DICT	5  */
"頻度ファイルではありません。",	/*WNN_NOT_A_HINDO_FILE	6  */
"付属語ファイルではありません。",	/*WNN_NOT_A_FUZOKUGO_FILE 7  */
"no_error 8",
"辞書テーブルが一杯です。",	/*WNN_JISHOTABLE_FULL	9*/
"頻度ファイルが、指定された辞書の頻度ファイルではありません。",
				 /*WNN_HINDO_NO_MATCH	10 */
"no_error 11",
"no_error 12",
"no_error 13",
"no_error 14",
"no_error 15",
"ファイルがオープンできません。",	/*WNN_OPENF_ERR		16*/
"正しい頻度ファイルではありません。",		/* WNN_NOT_HINDO_FILE	17 */
"正しい付属語ファイルではありません。",	/* WNN_NOT_FZK_FILE 18 */
"付属語の個数、ベクタ長さなどが多過ぎます。",	/* WNN_FZK_TOO_DEF 19 */
"その番号の辞書は、使われていません。",	/*WNN_DICT_NOT_USED	20 */
"no_error 21",
"no_error 22",
"no_error 23",
"付属語ファイルの内容が正しくありません", /* WNN_BAD_FZK_FILE  24     */
"疑似品詞番号が異常ですhinsi.dataが正しくありません。", /*WNN_GIJI_HINSI_ERR 25 */
"未定義の品詞が前端品詞として定義されています。", /*WNN_NO_DFE_HINSI 26*/
"付属語ファイルが読み込まれていません。", /*WNN_FZK_FILE_NO_LOAD	27 */
"no_error 28",
"no_error 29",
"辞書のエントリが多過ぎます。",		/*WNN_DIC_ENTRY_FULL	30 */
"変換しようとする文字列が長過ぎます。",	/*WNN_LONG_MOJIRETSU	31 */
"付属語解析領域が不足しています。",	/*WNN_WKAREA_FULL	32 */
"no_error 33",
"候補が多過ぎて次候補が取り出せません。",	/* WNN_JKTAREA_FULL 34 */
"指定された単語が存在しません。",	/* WNN_NO_KOUHO 35 */
"no_error 36",
"no_error 37",
"no_error 38",
"no_error 39",
"読みが長過ぎます。",			/*WNN_YOMI_LONG	40 */
"漢字が長過ぎます。",			/*WNN_KANJI_LONG	41 */
"指定された辞書は、登録可能ではありません。", /*WNN_NOT_A_UD	42 */
"登録する読みが入力されていません。",		/*WNN_NO_YOMI	43 */
"指定された辞書は、逆引き可能ではありません。", /* WNN_NOT_A_REV	44 */
"リードオンリーの辞書のエントリは登録/削除できません。",	/*WNN_RDONLY	45 */
"環境に辞書が存在しません。", /* WNN_DICT_NOT_IN_ENV 46 */
"no_error 47",
"no_error 48",
"リードオンリーの頻度は変更できません。", /* WNN_RDONLY_HINDO 49 */
"指定された単語が存在しません。",	/*WNN_WORD_NO_EXIST	50 */
"no_error 51",
"no_error 52",
"no_error 53",
"no_error 54",
"no_error 55",
"no_error 56",
"no_error 57",
"no_error 58",
"no_error 59",
"メモリを確保できません。",		/*WNN_MALLOC_INITIALIZE	60 */
"no_error 61",
"no_error 62",
"no_error 63",
"no_error 64",
"no_error 65",
"no_error 66",
"no_error 67",
"何かのエラーが起こりました。",		/* WNN_SOME_ERROR 68 */
"バグが発生している模様です。",		/*WNN_SONOTA 69 */
"サーバが動作していません。",		/*WNN_JSERVER_DEAD 70 */
"メモリを確保できません。",	/*WNN_ALLOC_FAIL	71 */
"サーバと接続できません。",	/*WNN_SOCK_OPEN_FAIL	72 */
"通信プロトコルのバージョンが合っていません。", /* WNN_BAD_VERSION	73 */
"クライアントの生成した環境ではありません。", /* WNN_BAD_ENV 74 */
"no_error 75",
"no_error 76",
"no_error 77",
"no_error 78",
"no_error 79",
"ディレクトリを作ることができません。",	/* WNN_MKDIR_FAIL	80	*/
"no_error 81",
"no_error 82",
"no_error 83",
"no_error 84",
"no_error 85",
"no_error 86",
"no_error 87",
"no_error 88",
"no_error 89",
"ファイルを読み込むことができません。",		/* WNN_FILE_READ_ERROR	90*/
"ファイルを書き出すことができません。",		/* WNN_FILE_WRITE_ERROR	91*/
"クライアントの読み込んだファイルではありません。",	/* WNN_FID_ERROR 92*/
"これ以上ファイルを読み込むことができません。",	/* WNN_NO_MORE_FILE	93*/
"パスワードが間違っています。",		/* WNN_INCORRECT_PASSWD    94   */
"ファイルが読み込まれています。 ", /*WNN_FILE_IN_USE    	95 */
"ファイルが削除できません。 ", /*WNN_UNLINK    		96 */
"ファイルが作成出来ません。", /*WNN_FILE_CREATE_ERROR	97 */
"Ｗｎｎのファイルでありません。", /*WNN_NOT_A_FILE		98	*/
"ファイルのI-nodeとFILE_UNIQを一致させる事ができません。", /*WNN_INODE_CHECK_ERROR   99 */

"品詞ファイルが大き過ぎます。",		/* WNN_TOO_BIG_HINSI_FILE 100 */
"品詞ファイルが大き過ぎます。",		/* WNN_TOO_LONG_HINSI_FILE_LINE 101  */
"品詞ファイルが存在しません。",		/* WNN_NO_HINSI_DATA_FILE 102 */
"品詞ファイルの内容が間違っています。",	/* WNN_BAD_HINSI_FILE 103 */
"no_error 104",
"品詞ファイルが読み込まれていません。",	/* WNN_HINSI_NOT_LOADED 105*/
"品詞名が間違っています。",		/* WNN_BAD_HINSI_NAME 106 */
"品詞番号が間違っています。",		/* WNN_BAD_HINSI_NO 107 */
"no_error 108",
"その操作はサポートされていません。", /*NOT_SUPPORTED_OPERATION 109 Not Used*/
"パスワードの入っているファイルがオープンできません。", /*WNN_CANT_OPEN_PASSWD_FILE 110  */
/* 初期化時のエラー  */
"uumrcファイルが存在しません。", /*WNN_RC_FILE_NO_EXIST    111 Not Used*/
"uumrcファイルの形式が誤っています。", /* WNN_RC_FILE_BAD_FORMAT  112 Not Used*/
"これ以上環境を作ることはできません。", /* WNN_NO_MORE_ENVS  113 */
"このクライアントが読み込んだファイルでありません。", /* WNN_FILE_NOT_READ_FROM_CLIENT 114 */
"辞書に頻度ファイルがついていません。", /*WNN_NO_HINDO_FILE 115 */
"パスワードのファイルが作成出来ません。" /*WNN_CANT_CREATE_PASSWD_FILE 116*/
};

int wnn_errormsg_len = sizeof(wnn_errormsg) / sizeof(char *);

extern	int	wnn_errorno;
static char msg[] = ":BAD ERRORNO!!! ";


char *wnn_perror()
{
static char msgarea[100];

  if((wnn_errorno < 0) || (wnn_errorno > sizeof(wnn_errormsg) / sizeof(char *))){
    sprintf(msgarea , "%d"  , wnn_errorno);
    strcat(msgarea , msg);
    return(msgarea);
  }
  return(wnn_errormsg[wnn_errorno]);
}
#endif	/* not_use */

extern struct msg_cat *wnn_msg_cat;

static char msg[] = ":BAD ERRORNO!!! ";

char *
wnn_perror_lang(lang, args)
char *lang;
ARGS *args;
{

    static char msgarea[100];
    char *x;

    LockMutex(&msg_lock);
    sprintf(msgarea , "%d"  , wnn_errorno);
    strcat(msgarea , msg);
    x = msg_get(wnn_msg_cat, wnn_errorno, msgarea, lang, args);
    UnlockMutex(&msg_lock);
    return x;
}

char *
wnn_perror(args)
ARGS *args;
{
    return(wnn_perror_lang(NULL, args));
}

/*
  Local Variables:
  eval: (setq kanji-flag t)
  eval: (setq kanji-fileio-code 0)
  eval: (mode-line-kanji-code-update)
  End:
*/
