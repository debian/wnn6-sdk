/*
 * $Id: fi_bdic.c,v 2.7.2.1 2000/08/04 05:37:05 kaneda Exp $
 */
/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/


/*
 * New FI-Wnn function call for both of server and library
 * Binary FI dictionary common routine
 */


#ifdef UX386
#include <X11/Xos.h>
#else
#include <sys/types.h>
#endif
#ifndef JS
#include <sys/stat.h>
#include <stdio.h>
#include "commonhd.h"
#include "jslib.h"
#include "jh.h"
#endif
#include "jdata.h"
#include "wnn_os.h"
#include "wnn_string.h"
#include "mt_jserver.h"
#include "fi_jdata.h"

#ifdef JS
static
#else
extern
#endif /* JS */
int output_header_fi_jt(), output_header_fi_hjt();

#ifndef JS
extern int getint(), getnstr(), input_file_uniq(), create_file_header();
extern void new_pwd();
extern
# ifdef BDIC_WRITE_CHECK
int
# else
void
#endif /* BDIC_WRITE_CHECK */
          putint(), put_n_str(), output_file_uniq(), put_n_EU_str(),
          putnull();
#endif /* Not JS */



#ifndef JS
/*:::DOC_START
 *
 *    Function Name: input_header_fi_jt
 *    Description  : ＦＩ関係辞書のヘッダ部を読み込む
 *    Parameter    :
 *         ifpter : (In) ファイルポインタ
 *         jt1 :    (Out) ＦＩ関係辞書へのポインタ
 *         args :   (InOut) マルチスレッド用構造体へのポインタ
 *
 *    Return value : 0==SUCCESS, -1==ERROR
 *
 *    Author      :  Hideyuki Kishiba
 *
 *    Revision history:
 *
 *:::DOC_END
 */
int
input_header_fi_jt(ifpter, jt1, args)
FILE *ifpter;
struct FI_JT *jt1;
ARGS *args;
{
    int i;
    if(
       getint(&jt1->syurui , ifpter, args) == -1 ||
       getint(&jt1->maxcomment, ifpter, args) == -1 || 
       getnstr(ifpter, WNN_PASSWD_LEN, jt1->hpasswd, args) == -1 ||
       getnstr(ifpter, 1, &jt1->maxjisho, args) == -1)
        return(-1);
    /* 接続定義Ｗｎｎ辞書 f_uniq 配列の読み込み */
    if(jt1->maxjisho) {
	if((jt1->jisho_uniq = (struct wnn_file_uniq *)
	    malloc(jt1->maxjisho * sizeof(struct wnn_file_uniq))) == NULL)
	     return(-1);
	for(i = 0; i < jt1->maxjisho; i++) {
	    if(input_file_uniq(&((jt1->jisho_uniq)[i]), ifpter, args) == -1)
	        return(-1);
	}
    }
    return(0);
} /* End of input_header_fi_jt */

/*:::DOC_START
 *
 *    Function Name: input_header_fi_hjt
 *    Description  : ＦＩ関係頻度のヘッダ部を読み込む
 *    Parameter    :
 *         ifpter : (In) ファイルポインタ
 *         jt1 :    (Out) ＦＩ関係頻度へのポインタ
 *         args :   (InOut) マルチスレッド用構造体へのポインタ
 *
 *    Return value : 0==SUCCESS, -1==ERROR
 *
 *    Author      :  Hideyuki Kishiba
 *
 *    Revision history:
 *
 *:::DOC_END
 */
int
input_header_fi_hjt(ifpter, hjt1, args)
FILE *ifpter;
struct FI_HJT *hjt1;
ARGS *args;
{
    if(
       input_file_uniq(&hjt1->fi_dic_uniq, ifpter, args) == -1 ||
       getint(&hjt1->maxcomment, ifpter, args) == -1 ||
       getnstr(ifpter, 1, &hjt1->maxjisho, args) == -1)
        return(-1);
    return(0);
} /* End of input_header_fi_hjt */

#endif /* Not JS */

/*:::DOC_START
 *
 *    Function Name: output_header_fi_jt
 *    Description  : ＦＩ関係辞書のヘッダ部を書き込む
 *    Parameter    :
 *         ofpter : (Out) ファイルポインタ
 *         jt1 :    (In) ＦＩ関係辞書へのポインタ
 *         args :   (InOut) マルチスレッド用構造体へのポインタ
 *
 *    Return value : 0==SUCCESS, -1==ERROR
 *
 *    Author      :  Hideyuki Kishiba
 *
 *    Revision history:
 *
 *:::DOC_END
 */
#ifdef JS
static
#endif
int
output_header_fi_jt(ofpter, jt1, args)
FILE *ofpter;
struct FI_JT *jt1;
ARGS *args;
{
    int i;
#ifdef BDIC_WRITE_CHECK
    if ((putint(ofpter, jt1->syurui, args) == -1) ||
	(putint(ofpter, jt1->maxcomment, args) == -1) ||
	(put_n_str(ofpter, jt1->hpasswd, WNN_PASSWD_LEN, args) == -1) ||
	(put_n_str(ofpter, &jt1->maxjisho, 1, args) == -1))
        return(-1);
    /* 接続定義Ｗｎｎ辞書 f_uniq 配列の書き込み */
    for(i = 0; i < jt1->maxjisho; i++) {
	if(output_file_uniq(&((jt1->jisho_uniq)[i]), ofpter, args) == -1)
	    return(-1);
    }
#else /* BDIC_WRITE_CHECK */
    putint(ofpter, jt1->syurui, args);
    putint(ofpter, jt1->maxcomment, args);
    put_n_str(ofpter, jt1->hpasswd, WNN_PASSWD_LEN, args);
    put_n_str(ofpter, &jt1->maxjisho, 1, args);
    /* 接続定義Ｗｎｎ辞書 f_uniq 配列の書き込み */
    for(i = 0; i < jt1->maxjisho; i++)
        output_file_uniq(&((jt1->jisho_uniq)[i]), ofpter, args);
#endif /* BDIC_WRITE_CHECK */
    return(0);
} /* End of output_header_fi_jt */

/*:::DOC_START
 *
 *    Function Name: output_header_fi_hjt
 *    Description  : ＦＩ関係頻度のヘッダ部を書き込む
 *    Parameter    :
 *         ofpter : (Out) ファイルポインタ
 *         hjt1 :   (In) ＦＩ関係頻度へのポインタ
 *         args :   (InOut) マルチスレッド用構造体へのポインタ
 *
 *    Return value : 0==SUCCESS, -1==ERROR
 *
 *    Author      :  Hideyuki Kishiba
 *
 *    Revision history:
 *
 *:::DOC_END
 */
#ifdef JS
static
#endif
int
output_header_fi_hjt(ofpter, hjt1, args)
FILE *ofpter;
struct FI_HJT *hjt1;
ARGS *args;
{
#ifdef BDIC_WRITE_CHECK
    if ((output_file_uniq(&hjt1->fi_dic_uniq, ofpter, args) == -1) ||
	(putint(ofpter, hjt1->maxcomment, args) == -1) ||
	(put_n_str(ofpter, &hjt1->maxjisho, 1, args) == -1))
        return(-1);
#else /* BDIC_WRITE_CHECK */
    output_file_uniq(&hjt1->fi_dic_uniq, ofpter, args);
    putint(ofpter, hjt1->maxcomment, args);
    put_n_str(ofpter, &hjt1->maxjisho, 1, args);
#endif /* BDIC_WRITE_CHECK */
    return(0);
} /* End of output_header_fi_hjt */

/*:::DOC_START
 *
 *    Function Name: create_fi_index_table
 *    Description  : ＦＩインデックステーブルを作成する
 *    Parameter    :
 * 	   fp :      (Out) ファイルポインタ
 *         njisho :  (In) 対応ＦＩ関係辞書の接続定義Ｗｎｎ辞書数
 *         primary : (In) 接続定義Ｗｎｎ辞書の primary index table 数の
 *		          配列へのポインタ
 *         args :    (InOut) マルチスレッド用構造体へのポインタ
 *
 *    Return value : 0==SUCCESS, -1==ERROR
 *
 *    Author      :  Hideyuki Kishiba
 *
 *    Revision history:
 *
 *:::DOC_END
 */
static int
create_fi_index_table(fp, njisho, primary, args)
FILE *fp;
unsigned char njisho;
int *primary;
ARGS *args;
{
    int i;
    
    for(i = 0; i < njisho; i++) {
#ifdef BDIC_WRITE_CHECK
	if((putint(fp, primary[i], args) == -1) ||
	   (putnull(fp, primary[i], args) == -1) ||
	   (putint(fp, 0, args) == -1))
	    return(-1);
#else /* BDIC_WRITE_CHECK */
	putint(fp, primary[i], args);
	putnull(fp, primary[i], args);
	putint(fp, 0, args);
#endif /* BDIC_WRITE_CHECK */
    }
    return(0);
} /* End of create_fi_index_table */

/*:::DOC_START
 *
 *    Function Name: create_fi_hindo_file
 *    Description  : ＦＩ関係頻度を作成するサブルーチン関数
 *    Parameter    :
 *	   funiq :   (In) 対応ＦＩ関係辞書の file uniq orig
 *	   fn :      (In) ＦＩ関係頻度ファイル名
 *  	   comm :    (In) ＦＩ関係頻度コメント
 *	   passwd :  (In) ＦＩ関係頻度パスワード
 * 	   njisho :  (In) 対応ＦＩ関係辞書の接続定義Ｗｎｎ辞書数
 *	   primary : (In) 接続定義Ｗｎｎ辞書 primary index table 数の
 *		          配列へのポインタ
 *	   args :    (InOut) マルチスレッド用構造体へのポインタ
 *
 *    Return value : 0==SUCCESS, -1==ERROR
 *
 *    Author      :  Hideyuki Kishiba
 *
 *    Revision history:
 *
 *:::DOC_END
 */
#ifdef JS
static
#endif
int
create_fi_hindo_file(funiq, fn, comm, passwd, njisho, primary, args)
struct wnn_file_uniq *funiq;
char *fn;
w_char *comm;
char *passwd;                   /* Not encoded */
unsigned char njisho;
int *primary;
ARGS *args;
{
    FILE *fp;
    struct FI_HJT hjt;
    char epasswd[WNN_PASSWD_LEN];
    w_char tmp[1];

    tmp[0] = 0;
    if(comm == NULL) comm = tmp;
    bcopy(funiq, &(hjt.fi_dic_uniq), WNN_F_UNIQ_LEN);
    hjt.maxcomment = wnn_Strlen(comm);
    hjt.maxjisho = njisho;

    if((fp = fopen(fn, "w+")) == NULL){
	return(-1);
    }
    if(passwd){
	new_pwd(passwd, epasswd);
    }else{
	bzero(epasswd, WNN_PASSWD_LEN);
    }
    if(create_file_header(fp, WNN_FT_FI_HINDO_FILE, epasswd, args) == -1) {
	fclose(fp);
	return(-1);
    }
#ifdef BDIC_WRITE_CHECK
    if ((output_header_fi_hjt(fp, &hjt, args) == -1) ||
	(put_n_EU_str(fp, comm, hjt.maxcomment, args) == -1) ||
	(create_fi_index_table(fp, njisho, primary, args) == -1)) {
	fclose(fp);
	return(-1);
    }
#else /* BDIC_WRITE_CHECK */
    output_header_fi_hjt(fp, &hjt, args);
    put_n_EU_str(fp, comm, hjt.maxcomment, args);
    create_fi_index_table(fp, njisho, primary, args);
#endif /* BDIC_WRITE_CHECK */

#ifdef  BSD42
    fchmod(fileno(fp), 0664);
    fclose(fp);
#else   /* SYSV */
    fclose(fp);
    chmod(fn, 0664);
#endif  /* BSD42 */
    return(0);
}

/*:::DOC_START
 *
 *    Function Name: create_null_fi_dic
 *    Description  : ＦＩ関係辞書を作成するサブルーチン関数
 *    Parameter    :
 *         fn :      (In) ＦＩ関係辞書ファイル名
 *         comm :    (In) ＦＩ関係辞書コメント
 *         passwd :  (In) ＦＩ関係辞書パスワード
 *	   hpasswd : (In) ＦＩ関係頻度パスワード
 *	   which :   (In) ＦＩ関係辞書タイプ(WNN_FI_USER_DICT)
 *         njisho :  (In) 接続定義Ｗｎｎ辞書数
 *	   duniq :   (In) 接続定義Ｗｎｎ辞書 file uniq orig の配列への
 *			  ポインタ
 *         primary : (In) 接続定義Ｗｎｎ辞書 primary index table 数の
 *	  	          配列へのポインタ
 *         args :    (InOut) マルチスレッド用構造体へのポインタ
 *
 *    Author      :  Hideyuki Kishiba
 */
#ifdef JS
static
#endif
int
create_null_fi_dic(fn, comm, passwd, hpasswd, which, njisho, duniq, primary, args)
char  *fn;
w_char *comm;
char *passwd, *hpasswd;  /* not encoded passwd */
int which;
unsigned char njisho;
struct wnn_file_uniq *duniq;
int *primary;
ARGS *args;
{
    FILE *fp;
    struct FI_JT jt;
    char epasswd[WNN_PASSWD_LEN];
    extern void new_pwd();

    if(hpasswd){
	new_pwd(hpasswd, jt.hpasswd);
    }else{
	bzero(jt.hpasswd, WNN_PASSWD_LEN);
    }
    jt.syurui = which;
    if(comm)
        jt.maxcomment = wnn_Strlen(comm);
    else
        jt.maxcomment = 0;
    jt.maxjisho = njisho;
    jt.jisho_uniq = duniq;

    if((fp = fopen(fn, "w+")) == NULL){
	return(-1);
    }
    if(passwd){
	new_pwd(passwd, epasswd);
    }else{
	bzero(epasswd, WNN_PASSWD_LEN);
    }
    if(create_file_header(fp, WNN_FT_FI_DICT_FILE, epasswd, args) == -1) {
	fclose(fp);
	return(-1);
    }
#ifdef BDIC_WRITE_CHECK
    if ((output_header_fi_jt(fp, &jt, args) == -1) ||
	(put_n_EU_str(fp, comm, jt.maxcomment, args) == -1) ||
	(create_fi_index_table(fp, njisho, primary, args) == -1)) {
	fclose(fp);
	return(-1);
    }
#else /* BDIC_WRITE_CHECK */
    output_header_fi_jt(fp, &jt, args);
    put_n_EU_str(fp, comm, jt.maxcomment, args);
    create_fi_index_table(fp, njisho, primary, args);
#endif /* BDIC_WRITE_CHECK */
    
#ifdef  BSD42
    fchmod(fileno(fp), 0664);
    fclose(fp);
#else   /* SYSV */
    fclose(fp);
    chmod(fn, 0664);
#endif  /* BSD42 */
    return(0);
} /* End of create_null_fi_dic */

