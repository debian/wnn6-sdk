/*
 * $Id: msg.c,v 2.11.2.2 2000/08/04 05:37:05 kaneda Exp $
 */
/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/
/*
	struct msg_cat msg_open(name, nlspath, lang)
	char *name;
	char *nlspath;
	char *lang;

	char * msg_get(cd, id, s, lang)
	struct msg_cat cd;
	int id;
	char *s;

	void msg_close(cd)
	struct msg_cat cd;

	format of message file
	    <message id>\t<message>
*/
#include <stdio.h>
#include "commonhd.h"
#include "dslib.h"
#include "wnn_os.h"
#include "msg.h"
#include "mt_jserver.h"


extern char *getenv();

static char *
bsearch(ky, bs, nel, width, compar)
char *ky;
char *bs;
unsigned long nel;
unsigned long width;
int (*compar)();
{
    char *key = ky;
    char *base = bs;
    int two_width = width + width;
    char *last = base + width * (nel - 1);

    register char *p;
    register int ret;

    while (last >= base) {
	p = base + width * ((last - base)/two_width);
	ret = (*compar)((void *)key, (void *)p);

	if (ret == 0)
	    return ((char *)p);	/* found */
	if (ret < 0)
	    last = p - width;
	else
	    base = p + width;
    }
    return ((char *) 0);	/* not found */
}

static char *
getlang(lang, args)
char *lang;
ARGS *args;
{
    static char tmp[32];
    char *p;
    int i;

    if(lang == NULL || *lang == '\0'){
#ifdef	HAS_SETLOCALE
        lang = setlocale(LC_ALL, NULL);
        if(lang == NULL || *lang == '\0')
#endif
	{
	    lang = getenv("LC_MESSAGES");
	    if(lang == NULL || *lang == '\0'){
		lang = getenv("LANG");
		if(lang == NULL || *lang == '\0'){
		    lang = DEF_LANG;
		}
	    }
	}
    }
    for (i = 0, p = lang; *p && (*p != '.')
	&& (i < sizeof(tmp) - 1)
	; i++, p++) {
	tmp[i] = *p;
    }
    tmp[i] = '\0';
    return(tmp);
    /*
    return(lang);
    */
}

static int
_search(id, bd)
register int id;
register struct msg_bd *bd;
{
    return(id - bd->msg_id);
}

static void
_escape(op, ip)
register char *op, *ip;
{
    for( ; *ip != 0; ip++, op++){
	if(*ip == '\\'){
	    switch(*++ip){
	    case 'n':
		*op = '\n';
		break;
	    case 't':
		*op = '\t';
		break;
	    case 'b':
		*op = '\b';
		break;
	    case 'r':
		*op = '\r';
		break;
	    case 'f':
		*op = '\f';
		break;
	    case 'v':
		*op = '\v';
		break;
	    case '0':
		*op = 0;
		break;
	    /*
	    case 'a':
		*op = '\a';
		break;
	    case 'e':
	    case 'E':
	    case 'o':
	    case 'd':
	    case 'x':
		break;
	    */
	    default:
		*op = *ip;
		break;
	    }
	} else {
	    if (*ip == '\n') {
		*op = '\0';
	    } else {
		*op = *ip;
	    }
	}
    }
    *op = 0;
}

static char *
get_msg_bd(cd, id)
register struct msg_cat *cd;
register int id;
{
    register struct msg_bd *bd;
    if(cd->msg_bd == 0 || cd->msg_cnt == 0)
	return(NULL);
    bd = (struct msg_bd *)
	bsearch(id, cd->msg_bd, cd->msg_cnt, sizeof(struct msg_bd), _search);
    if(bd == NULL)
	return(NULL);
    return(bd->msg);
}

/* expand
	%N: the value of the name parameter passed to msg_open()
	%L: the value of LANG
	%l: the language element from LANG
	%t: the territory element from LANG
	%c: the codeset element from LANG
	%%: a single % charctor 
*/
static int
expand(op, ip, name, lang)
register char *op, *ip, *name, *lang;
{
     if (!ip || !*ip) return(-1);
     for( ; *ip != 0; ip++){
	if(*ip == '%'){
	    switch(*++ip){
	    case 'N':
		if (!name || !*name) return(-1);
		strcpy(op, name);
		op += strlen(name);
		break;
	    case 'L':
		if (!lang || !*lang) return(-1);
		strcpy(op, lang);
		op += strlen(lang);
		break;
	    /*
	    case 'l':
		strcpy(op, language);
		op += strlen(language);
		break;
	    case 't':
		strcpy(op, terr);
		op += strlen(terr);
		break;
	    case 'c':
		strcpy(op, code);
		op += strlen(code);
		break;
	    case '%':
		strcpy(op, "%");
		op += strlen("%");
		break;
	    */
	    default:
		break;
	    }
	 }else{
	    *op = *ip;
	    op++;
	 }
     }
     *op = '\0';
     return(0);
}


struct msg_cat *
msg_open(name, nlspath, lang, args)
char *name;
char *nlspath;
char *lang;
ARGS *args;
{
    struct msg_cat *cd;

    char fn[128];
    FILE *fp;
    char data[1024];
    char save[1024];
    int msg_cnt = 0;
    int msg_byte = 0;
    register char *dp;
    register struct msg_bd *bd;
    register char *msg, *l;

    l = getlang(lang, args);
    if (name && *name == '/') {
	strcpy(fn, name);
    } else {
	if (expand(fn, nlspath, name, l) == -1) {
	    return(NULL);
	}
    }

    if (!(cd = (struct msg_cat *)malloc(sizeof(struct msg_cat))))
	return(NULL);

    strcpy(cd->name, name);
    strcpy(cd->lang, l);
    strcpy(cd->nlspath, nlspath);
    cd->nextp = NULL;
    cd->msg_cnt = 0;

    if((fp = fopen(fn, "r")) == NULL){
	/* message file not found */
	cd->msg_bd = 0;
	return(cd);
    }
    for( ; ; ){
	/* first: count bytes */
	if(fgets(data, 1024, fp) == NULL)
	    break;
	if(*data == '#')
	    continue;	/* comment */
	for(dp = data ; *dp != '\t'; dp++);	/* msg_id:message\n */
	dp++;
	msg_byte += strlen(dp);
	msg_cnt++;
    }
    rewind(fp);

    cd->msg_cnt = msg_cnt;
    if (!(bd = cd->msg_bd = (struct msg_bd *)
	       malloc((sizeof(struct msg_bd)) * msg_cnt + msg_byte + 1))) {
	fclose(fp);
	free(cd);
	return(NULL);
    }
    msg = (char *) bd + (sizeof(struct msg_bd)) * msg_cnt;

    for( ; ; ){
	/* second : get message */
	if(fgets(data, 1024, fp) == NULL)
	    break;
	if(*data == '#')
	    continue;	/* comment */
	for(dp = data ; *dp != '\t'; dp++);	/* msg_id:message\n */
	*dp = 0;
	dp++;
	bd->msg_id = atoi(data);
	bd->msg = msg;
	bd++;
	_escape(save, dp);
	strcpy(msg, save);
	msg += strlen(save);
	*msg = 0;
	msg++;
    }
    fclose(fp);
    return(cd);
}

char *
msg_get(catd, id, msg, lang, args)
struct msg_cat *catd;
int	id;
char	*msg;
register char	*lang;
ARGS *args;
{
    register struct msg_cat *cd;
    static char ret[128];
    register char *msg_bd;

    if(catd == 0)
	goto error;
    cd = catd;
    if(lang == 0 || *lang == '\0'){
	lang = cd->lang;
    } else {
	for ( ; ; cd = cd->nextp) {
	    if(strcmp(lang, cd->lang) == 0)
		break;
	    if(cd->nextp == 0) {
		cd->nextp = msg_open(cd->name, cd->nlspath, lang, args);
		cd = cd->nextp;
		break;
	    }
	}
    }

    if((msg_bd = get_msg_bd(cd, id)))
	return(msg_bd);
error:
    if(msg != 0 && *msg != '\0')
	return(msg);
    sprintf(ret, "mes_id = %d: %s", id, DEF_MSG);
    return(ret);
}

void
msg_close(cd)
register struct msg_cat *cd;
{
    if(cd->nextp)
	msg_close(cd->nextp);
    if(cd->msg_bd)
	free(cd->msg_bd);
    if(cd)
	free(cd);
}

#ifdef	not_use
/* test */
main()
{
	struct msg_cat *cd;

	cd = msg_open("msg","%L", "ja_JP", NULL);

	printf(msg_get(cd, 5, "message not found\n", "ja_JP", NULL), 555);
	printf(msg_get(cd, 6, "message not found\n", "zh_CN", NULL));
	printf(msg_get(cd, -1, "", "ja_JP", NULL), 555);
	printf(msg_get(cd, 2, "message not found\n", "ja_JP", NULL), "abc");
	printf(msg_get(cd, 100, "message not found\n", "zh_CN", NULL), "abc");
}
#endif	/* not_use */
