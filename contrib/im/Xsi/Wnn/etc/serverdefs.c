/*
 * $Id: serverdefs.c,v 2.6.2.1 2000/08/04 05:37:06 kaneda Exp $
 */
/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/
#include <stdio.h>
#include <ctype.h>
#ifdef UX386
#include <X11/Xos.h>
#else
#include <fcntl.h>
#endif
#include "commonhd.h"
#include "config.h"
#include "serverdefs.h"

#define MACHINE_NAME		1
#define UNIXDOMAIN_NAME		2
#define SERVICE_NAME		3
#define SERVICE_PORT_NUM	4
#define SERVERENV_NAME		5

typedef struct _server_env_struct {
    char *lang;
    char *env;
} server_env_struct;

static server_env_struct server_env[] = {
    {WNN_J_LANG, WNN_JSERVER_ENV},
    {WNN_C_LANG, WNN_CSERVER_ENV},
    {WNN_K_LANG, WNN_KSERVER_ENV},
    {WNN_T_LANG, WNN_TSERVER_ENV},
    {NULL, NULL}
};

static char *
get_default_server_env(lang)
register char *lang;
{
    register server_env_struct *p;

    if (!lang || !*lang) return(NULL);

    for (p = server_env; p->lang; p++) {
	if (!strncmp(lang, p->lang, strlen(lang))) {
	    return(p->env);
	}
    }
    return(NULL);
}

static char *
get_serverdefs(lang, cnt)
char *lang;
int cnt;
{
    FILE *fp;
    static char s[7][EXPAND_PATH_LENGTH];
    char serv_defs[EXPAND_PATH_LENGTH];
    char data[1024];
    int num;

    strcpy(serv_defs, LIBDIR);
    strcat(serv_defs, SERVERDEFS_FILE);
    if ((fp = fopen(serv_defs, "r")) == NULL) {
	return(NULL);
    }
    if (!lang || !*lang) lang = "ja_JP";
    while (fgets(data, 1024, fp) != NULL) {
	num = sscanf(data, "%s %s %s %s %s %s %s",
		     s[0],s[1],s[2],s[3],s[4],s[5],s[6]);
	if ((num < 4) || s[0][0] == ';') { 
	    continue;
	}
	if (!strncmp(lang, s[0], strlen(s[0]))) {
	    fclose(fp);
	    if (cnt >= num) return(NULL);
	    if (strlen(s[cnt]) == strlen("NULL") && !strcmp(s[cnt], "NULL")) {
		return(NULL);
	    } else {
		return(s[cnt]);
	    }
	}
    }
    fclose(fp);
    return(NULL);
}

char *
wnn_get_machine_of_serverdefs(lang)
char *lang;
{
    return(get_serverdefs(lang, MACHINE_NAME));
}

char *
wnn_get_unixdomain_of_serverdefs(lang)
char *lang;
{
    return(get_serverdefs(lang, UNIXDOMAIN_NAME));
}

char *
wnn_get_service_of_serverdefs(lang)
char *lang;
{
    return(get_serverdefs(lang, SERVICE_NAME));
}

int
wnn_get_port_num_of_serverdefs(lang)
char *lang;
{
    char *port_char;

    if ((port_char = get_serverdefs(lang, SERVICE_PORT_NUM)) == NULL) {
	return(-1);
    } else {
	return(atoi(port_char));
    }
}

char *
wnn_get_serverenv_of_serverdefs(lang)
char *lang;
{
    char *serverenv;

    if (!(serverenv = get_serverdefs(lang, SERVERENV_NAME))) {
	return(get_default_server_env(lang));
    } else {
	return(serverenv);
    }
}
