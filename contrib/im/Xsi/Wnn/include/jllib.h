/*
 * $Id: jllib.h,v 2.22.2.1 2000/08/04 05:37:14 kaneda Exp $
 */

/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/

/*	Version 4.0
 */
/*
	Nihongo	Henkan	Library Header File
*/

#ifndef _JLLIB_H_
#define _JLLIB_H_

#include "jslib.h"

/*
   ユーザープログラムは、直接この構造体の内容をアクセスする必要は無いはず。
   ライブラリ関数を用いること。
*/

#define WNN_YOMI_SIZE 10

typedef struct wnn_jl_bun WNN_BUN;

struct wnn_jl_bun {
	int	jirilen;	/* 候補文節の自立語 */
	int	dic_no;
	int	entry;
	int	kangovect;	/* 接続ベクトルテーブルへのポインタ */
	int	hinsi;		/* 品詞 */
	int	fukugou;	/* 複合語優先変換 */
	int     num_hinsi;      /* 品詞指定変換で使用した品詞の数 */
	int	*hinsi_list;	/* 品詞指定変換で使用した品詞番号リスト */
	int	hindo  :16;		/* 頻度(仮想値) */
	int	ref_cnt :4;		/* 文節リストに使われている */
	/* BUG FIX signed --> unsigned */
	unsigned int ima :1;		/* 今使ったよビット */
	unsigned int hindo_updated :1;    /* この文節の頻度は更新されている */
	unsigned int nobi_top :1;	/* 伸ばし縮みを行ったかを示す。*/
	/* egg みたいに伸ばし縮みを繰り返し行う時に、途中に現れる候補の今
	   使ったよ ビットを 落としてはならない。*/
	unsigned int dai_top :1;	/* 大文節の先頭 */
	unsigned int dai_end :1; /* 大文節のsaigo *//* 次候補 nomi */
	unsigned int from_zenkouho :3;	/*次候補から選択された文節かどうか */
	unsigned int bug :1;			/* BUG FIX とりあえず */
	/* BUG FIX signed --> unsigned */
	int 	hyoka;
	int 	daihyoka;
	short yomilen;
	short kanjilen;
	short real_kanjilen;
	WNN_BUN *down; /* 今使ったよビットを落とす対象の文節 */
	w_char   yomi[WNN_YOMI_SIZE];        /* 読み、漢字が入る。入り切らない時には
				    次につなげる */
	WNN_BUN *next;		/* 次のストラクトへのポインタ */
	WNN_BUN *free_next;		/* 次のストラクトへのポインタ */
};
    

struct wnn_buf {
    struct wnn_env *env;	/* 環境 */
    int bun_suu;		/* 文節数 */
    int zenkouho_suu;		/* 全候補の数 */
    WNN_BUN **bun;		/* 文節へのポインタ */
    WNN_BUN **down_bnst; /* 今使ったよビットを落とす対象の文節 */

    WNN_BUN **zenkouho;		/* 全候補へのポインタ */
    int *zenkouho_dai;		/* daibunsetsu zenkouho */
    int zenkouho_dai_suu;	/* daibunsetsu zenkouho suu */

    short c_zenkouho;		/* 全候補中、注目している候補 */
    short zenkouho_daip;
    int zenkouho_bun;		/* 全候補を取っている文節 */
    int zenkouho_end_bun;	/* 全候補(dai)を取っている最後の文節 */
    int zenkouho_endvect;	/* 全候補を取った endvect *//* ADD KURI */
    
    WNN_BUN *free_heap;
    char *heap;		/* linked list of alloced area */
    int msize_bun;
    int msize_zenkouho;
    struct _WnnAutoTune *at;

    /*
     * Hideyuki Kishiba (Sep. 20, 1994)
     * 使用ＦＩ関係送受信用構造体をメンバに加える
     */
    struct wnn_fi_rel_buf fi_rb;

    /* 
     * Hideyuki Kishiba (Nov. 24, 1994)
     * 直前に確定した文節情報を覚えておく
     */
   struct wnn_prev_bun prev_bun[WNN_PREV_BUN_SUU];
};


#define WNN_SHO 0
#define WNN_DAI 1
#define WNN_IKEIJI 2

#define WNN_ZIP 1
#define WNN_TEL 2
#define WNN_TANKAN 3

#define WNN_NO_USE    0
#define WNN_USE_MAE   1
#define WNN_USE_ATO   2
#define WNN_USE_ZENGO (WNN_USE_MAE | WNN_USE_ATO)
/* #define WNN_ZENGO_YUUSEN 4 */

#define WNN_UNIQ_KNJ 2
#define WNN_UNIQ 1
#define WNN_NO_UNIQ  0

#define WNN_NO_CREATE 0
#define WNN_CREATE (-1)

#define WNN_DIC_PRIO_DEFAULT 5

#define WNN_YOMI 0
#define WNN_KANJI 1

#define WNN_KATAKANA_LEARNING_LEN	3	/* minimum length of auto
						   learning katakana	*/

#define jl_next(buf) jl_set_jikouho((buf), (buf)->c_zenkouho + 1)

#define jl_previous(buf) jl_set_jikouho((buf), (buf)->c_zenkouho - 1)

#define jl_next_dai(buf) jl_set_jikouho_dai((buf), (buf)->c_zenkouho + 1)

#define jl_previous_dai(buf) jl_set_jikouho_dai((buf), (buf)->c_zenkouho - 1)

#define jl_get_kanji(buf, bun_no, bun_no2, area)\
                   wnn_get_area(buf, bun_no, bun_no2, area, WNN_KANJI)

#define jl_get_yomi(buf, bun_no, bun_no2, area)\
                   wnn_get_area(buf, bun_no, bun_no2, area, WNN_YOMI)

#define jl_fuzoku_len(buf, k) \
		(jl_yomi_len((buf), k, k+1) - jl_jiri_len((buf), k))

#define jl_jiri_kanji_len(buf, k) \
		(jl_kanji_len((buf), k, k+1) - jl_fuzoku_len((buf), k))

/* Macros to create library functions from "_e" libraries */

/* for select server by useing $LANG */
#define	jl_open(env_n, server_n, wnnrc_n, error_handler, message_handler, timeout) \
    jl_open_lang(env_n, server_n, NULL, wnnrc_n, error_handler, message_handler, timeout)

#define	jl_connect(env_n, server_n, wnnrc_n, error_handler, message_handler, timeout) \
    jl_connect_lang(env_n, server_n, NULL, wnnrc_n, error_handler, message_handler, timeout)

/* extern function defines */
extern struct wnn_buf *jl_open_lang();
extern struct wnn_env *jl_connect_lang();
extern struct wnn_env *jl_env_get();
extern struct wnn_jdata *jl_word_info_e();
extern w_char *jl_hinsi_name_e();

extern void jl_close();
extern int jl_dic_add_e();
extern int jl_dic_comment_set_e();
extern int jl_dic_delete_e();
extern int jl_dic_list_e();
extern int jl_dic_save_all_e();
extern int jl_dic_save_e();
extern int jl_dic_use_e();
extern void jl_disconnect();
extern void jl_disconnect_if_server_dead();
extern void jl_env_set();
extern int jl_fuzokugo_get_e();
extern int jl_fuzokugo_set_e();
extern void jl_get_zenkouho_kanji();
extern int jl_hinsi_dicts_e();
extern int jl_hinsi_list_e();
extern int jl_hinsi_number_e();
extern int jl_isconnect_e();
extern int jl_kanji_len();
extern int jl_kill();
extern int jl_nobi_conv();
extern int jl_nobi_conv_e2();
extern int jl_param_get_e();
extern int jl_param_set_e();
extern int jl_ren_conv();
extern int jl_set_env_wnnrc();
extern int jl_set_env_wnnrc1();
extern int jl_set_jikouho();
extern int jl_set_jikouho_dai();
extern int jl_tan_conv();
extern int jl_update_hindo();
extern int jl_word_add_e();
extern int jl_word_comment_set_e();
extern int jl_word_delete_e();
extern int jl_word_search_by_env_e();
extern int jl_word_search_e();
extern int jl_word_use_e();
extern int jl_yomi_len();
extern int jl_zenkouho();
extern int jl_zenkouho_dai();
extern int wnn_get_area();

extern int jl_isconnect();
extern int jl_dic_add();
extern int jl_dic_delete();
extern int jl_fuzokugo_set();
extern int jl_fuzokugo_get();
extern int jl_dic_save();
extern int jl_dic_save_all();
extern struct wnn_jdata *jl_word_info();
extern int jl_dic_list();
extern int jl_word_search();
extern int jl_word_search_by_env();
extern int jl_word_use();
extern int jl_param_set();
extern int jl_param_get();
extern int jl_dic_use();
extern int jl_word_add();
extern int jl_word_delete();
extern int jl_hinsi_number();
extern w_char *jl_hinsi_name();
extern int jl_hinsi_list();
extern int jl_hinsi_dicts();
extern int jl_word_comment_set();
extern int jl_dic_comment_set();
extern struct wnn_jdata *jl_inspect();
extern int jl_env_sticky_e();
extern int jl_env_sticky();
extern int jl_env_un_sticky_e();
extern int jl_env_un_sticky();
extern char *jl_get_lang();
extern int jl_bun_suu();
extern int jl_zenkouho_suu();
extern int jl_zenkouho_bun();
extern int jl_c_zenkouho();
extern int jl_zenkouho_daip();
extern int jl_dai_top();
extern int jl_jiri_len();
extern struct wnn_env *jl_env();

extern int jl_get_wnn_errorno_env();
extern int jl_get_wnn_errorno_buf();
extern int jl_tan_conv_hinsi_flag();
extern int jl_nobi_conv_hinsi_flag();
extern int jl_zenkouho_hinsi_flag();
extern int jl_zenkouho_dai_hinsi_flag();
extern int jl_ren_conv_with_hinsi_name();
extern int jl_tan_conv_with_hinsi_name();
extern int jl_nobi_conv_with_hinsi_name();
extern int jl_zenkouho_with_hinsi_name();
extern int jl_zenkouho_dai_with_hinsi_name();

extern int jl_set_henkan_env_e();
extern int jl_set_henkan_env();
extern int jl_get_henkan_env_e();
extern int jl_get_henkan_env();
extern int jl_get_henkan_env_local_e();
extern int jl_get_henkan_env_local();
extern int jl_set_henkan_hinsi_e();
extern int jl_set_henkan_hinsi();
extern int jl_get_henkan_hinsi_e();
extern int jl_get_henkan_hinsi();


/*
 * Hideyuki Kishiba (Jul. 8, 1994)
 * New jl_lib functions for FI-Wnn
 */
extern int jl_fi_dic_add_e();
extern int jl_fi_dic_add();

extern int jl_fi_ren_conv();
extern int jl_fi_nobi_conv();
extern int jl_optimize_fi();
extern int jl_reset_prev_bun();

extern int jl_fi_dic_list_e();
extern int jl_fi_dic_list();
extern int jl_fuzokugo_list_e();
extern int jl_fuzokugo_list();
extern int jl_free();

extern char *wnn_perror();
extern char *wnn_perror_lang();

#ifdef  CONVERT_by_STROKE
extern void jl_get_zenkouho_yomi();
#endif

extern int jl_zenikeiji_dai();
extern int jl_set_ikeiji_dai();

#endif	/* _JLLIB_H_ */
