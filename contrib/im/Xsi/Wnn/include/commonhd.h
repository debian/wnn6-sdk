/*
 * $Id: commonhd.h,v 2.30.2.3 2000/08/04 05:37:11 kaneda Exp $
 */
/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/

/*	Version 4.0
 */
/****************
 * Common header 
 ****************/
#ifndef _COMMONHD_H_
#define _COMMONHD_H_

#include <stdio.h>
#ifdef linux
#include <unistd.h>
#endif
#define	JSERVER_VERSION	0xF101	/* minor version */

#define	_SERVER_VERSION	"Wnn6 R2.22" /* other */

#include "wnnerror.h"

#ifndef JS
typedef  unsigned int  UINT;
typedef  unsigned char UCHAR;
#ifndef	w_char
# define w_char unsigned short
#endif	/* w_char */
#endif /*JS */

#ifdef TAIWANESE
#ifndef	CHINESE
#define CHINESE
#endif
#endif

#ifdef	CHINESE
#define	CONVERT_from_TOP
#define	CONVERT_by_STROKE	/* 筆形(Bi Xing) */
#define	CONVERT_with_SiSheng	/* 四声(Si Sheng) */
#define NO_FZK			/* 付属語は、ない */
#define NO_KANA			/* ひらがな(読みと同じ候補)は、ない */
#endif

#ifdef KOREAN
#define CONVERT_from_TOP
#define NO_FZK
#endif

#ifdef luna
#ifdef uniosu
# ifndef	SYSVR2
#  define	SYSVR2
# endif
# ifndef	TERMINFO
#  define	TERMINFO
# endif
#else	/* if defined(MACH) || defined(uniosb) */
# ifndef	BSD42
#  define	BSD42
# endif
# ifndef	BSD43
#  define	BSD43
# endif
#  if defined(luna68k)
#   ifndef	BSD44
#    define	BSD44
#   endif
#  endif /* defined(luna68k) */
# ifndef	TERMCAP
#  define	TERMCAP
# endif
#endif
#else	/* defined(luna) */
#if defined(sun) && !defined(SVR4)
# ifndef	BSD42
#  define	BSD42
# endif
# ifndef	TERMCAP
#  define	TERMCAP
# endif
#else	/* sun else */
#if defined(DGUX) || defined(linux)
# ifndef	SYSVR2
#  define	SYSVR2
# endif
# ifndef	TERMCAP
#  define	TERMCAP
# endif
#else
#if defined(SVR4) || defined(hpux) || defined(AIXV3) || defined(SGI)
# ifndef	SYSVR2
#  define	SYSVR2
# endif
# ifndef	TERMINFO
#  define	TERMINFO
# endif
# if (defined solaris || defined SOLARIS)
#  define   BSD_COMP
# endif
#else
# ifndef	BSD43
#  define	BSD43
# endif
# ifndef	BSD42
#  define	BSD42
# endif
# ifndef	TERMCAP
#  define	TERMCAP
# endif
#endif /* defined(SVR4) || defined(hpux) */
#endif /* DGUX */
#endif /* sun */
#endif /* luna */

#if defined(SYSVR2) || defined(sun)
# define SRAND48
#endif
#if defined(SVR4) || defined(hpux)
#ifndef F_OK
#define F_OK	0
#endif
#ifndef R_OK
#define R_OK	4
#endif
#endif

#define MAXBUNSETSU	80
#define LIMITBUNSETSU   600
#define MAXJIKOUHO	600

#define J_IUJIS 0
#define J_EUJIS 1
#define J_JIS   2
#define J_SJIS  3

#define C_IUGB	0
#define C_EUGB	1

#define C_ICNS11643  0
#define C_ECNS11643  1
#define C_BIG5  2

#define K_IUKSC 0
#define K_EUKSC 1
#define K_KSC	2

#ifndef	True
#define	True	1
#endif
#ifndef	False
#define	False	0
#endif

#define KANJI(x)	((x) & 0x80)


#define Ctrl(X)		((X) & 0x1f)

#define NEWLINE		Ctrl('J')
#define CR		Ctrl('M')
#define ESC		'\033'

/* rubout_code が 0x08 であることが前提で作られている様なので変更した */
#define RUBOUT		0x08
#if 0
#ifdef luna
#ifdef uniosu
#define RUBOUT		0x08	/* BS */
#else
#define RUBOUT		'\177'
#endif
#else
#define RUBOUT		'\177'
#endif
#endif
#define SPACE		' '


#define JSPACE		0xa1a1
#ifdef KOREAN
#define BAR		0xA1aa	/* ー	*/
#else
#define BAR		0xA1BC	/* ー	*/
#endif
#define KUTEN_NUM	0xA1A3	/* 。	*/
#define TOUTEN_NUM	0xA1A2	/* 、	*/
#define S_NUM		0xA3B0	/* ０	*/
#define E_NUM		0xA3B9	/* ９	*/
#ifdef KOREAN
#define S_HIRA		0xAAA1	/* ぁ	*/
#define E_HIRA		0xAAF3	/* ん	*/
#define S_KATA		0xABA1	/* ァ	*/
#define E_KATA		0xABF6	/* ヶ	*/
#else
#define S_HIRA		0xA4A1	/* ぁ	*/
#define E_HIRA		0xA4F3	/* ん	*/
#define S_KATA		0xA5A1	/* ァ	*/
#define E_KATA		0xA5F6	/* ヶ	*/
#endif
#define S_HANKATA	0x00A1	/* 治	*/
#define E_HANKATA	0x00DF	/* 釈    */

#ifdef KOREAN
#define S_JUMO		0xa4a1	/* ぁ */
#define E_JUMO		0xa4fe	/* �� */
#define S_HANGUL	0xb0a1	/* 亜 */
#define E_HANGUL	0xc8fe	/* 美 */
#define S_HANJA		0xcaa1	/* 福 */
#define E_HANJA		0xfdfe	/* �� */

#define ishanja(x)      ((unsigned)((x) - S_HANJA) <= (E_HANJA - S_HANJA))
#define ishangul(x)     ((unsigned)((x) - S_HANGUL) <= (E_HANGUL - S_HANGUL))
#endif

#define HIRAP(X) ((X) >= S_HIRA && (X) <= E_HIRA)
#define KATAP(X) (((X) >= S_KATA && (X) <= E_KATA) || ((X) == BAR))
#define ASCIIP(X) ((X) < 0x7f)
#define KANJIP(X) (!(HIRAP(X) || KATAP(X) || ASCIIP(X)))

#define YOMICHAR(X) ((HIRAP(X)) || \
		     ('0'<=(X)&&'9'>=(X)) || \
		     ('A'<=(X)&&'Z'>=(X)) || \
		     ('a'<=(X)&&'z'>=(X)) || \
		     (BAR == X) \
		    )
#define HIRA_OF(X) ((KATAP(X) && !(BAR == (X)))? ((X) & ~0x0100) : (X))

#ifdef	CONVERT_by_STROKE
# define Q_MARK		'?'
#endif	/* CONVERT_by_STROKE */

#define LENGTHYOMI      256	/* jisho ni touroku suru yomi no nagasa */
#define LENGTHKANJI     256	/* jisho ni touroku suru kanji no nagasa */
#define LENGTHBUNSETSU  264    /* 文節の最大長 */
#define LENGTHCONV      512	/* 変換可能最大文字数 */

#define JISHOKOSUU     20	

#define DIC_RDONLY 1   /* 辞書がリード・オンリーである。*/


/* 複数のファイルにまたがって用いられているバッファサイズの定義 */
#define EXPAND_PATH_LENGTH 256 /* expand_expr()が用いるバッファのサイズ */


#define WNN_FILE_STRING "Ｗｎｎのファイル"

#define WNN_FILE_STRING_LEN 16


#define F_STAMP_NUM 64
#define FILE_ALREADY_READ -2
#define FILE_NOT_READ -3



#endif /* _COMMONHD_H_ */
/*
  Local Variables:
  kanji-flag: t
  End:
*/

