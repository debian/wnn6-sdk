/*
 * $Id: cplib.h,v 2.2.2.1 2000/08/04 05:37:12 kaneda Exp $
 */
/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/
/**  cWnn  Version 1.1	 **/
#ifndef _CPLIB_H_
#define _CPLIB_H_

#ifndef min
#define min(a,b) ((int)(a) > (int)(b)? (b):(a))
#endif 

#define YIN_LEN         10
#define PY_LEN          10      /*   'Chuang��', 'Zhuang��'  */
#define PY_LEN_W        7       /* for w_char  */

#define PY_MARK       'P'   /* for PinYincode: to know if need to change to */
#define ZY_MARK       'Z'   /* for PinYincode: to know if need to change to */
#define CWNN_PINYIN	0	/* For Pinyin */
#define CWNN_ZHUYIN	1	/* For Zhuyin */


#define  PY_EOF 	0x8ec0  /* PY_EOF is end charactor of one PinYin
                       		         must be a 2 bytes code.  HUANG */
#define  ZY_EOF_0	0x8ec0  /* ZhuYin end character(no sisheng): '��' */
#define  ZY_EOF_1	0x8ec1	/* ZhuYin end character(sisheng 1 ): '��' */
#define  ZY_EOF_2	0x8ec2	/* ZhuYin end character(sisheng 2 ): '��' */
#define  ZY_EOF_3	0x8ec3	/* ZhuYin end character(sisheng 3 ): '��' */
#define  ZY_EOF_4	0x8ec4	/* ZhuYin end character(sisheng 4 ): '��' */

#define  isZY_EOF(X)  ( ((int)(X) >= ZY_EOF_0 && (int)(X) <= ZY_EOF_4 )? 1 : 0 )

#define PY_NUM_SHENGMU	24		/* ShengMu table size of PinYin */
#define PY_NUM_YUNMU	39		/* YunMu table size of PinYin */
#define ZY_NUM_SHENGMU	24		/* ShengMu table size of ZhuYin */
#define ZY_NUM_YUNMU	41		/* YunMu table size of ZhuYin */

#define EMPTY_SHENG_RAW 0		/* position of ShengMu EMPTY in ShengMu
					   table  */
#define EMPTY_YUN_RAW 0			/* position of YunMu EMPTY in YunMu
					   table  */
#define X_SHENG_RAW	20		/* position of ShengMu X in ShengMu 
					   table  */

/* YINcode creating is based on PinYin */
/* isyincod_d():  Check it is in the domain of Pinyin. To check if it is a 
   Pinyin, you need to use cp_isyincod() wihich checks the PinYin table  */
#define  _cwnn_isyincod_d(c)  ( ((c & 0x80) &&		\
				(!(c & 0x8000)) &&		\
				(c & 0x7f) >= 0x20 &&		\
				(((int)c >> (int)8) & 0x7f) >= 0x20 )	\
			      ?1:0 )	/* if is in YINcode's limite */

#define _cwnn_sisheng(YINcod)	( ((YINcod & 0x100) == 0x100)?  \
			   ((YINcod & 0x03 ) + 1): 0 )

#define _cwnn_yincod_0(YINcod)    ((YINcod) & 0xfefc)

/* Shengraw based on PinYin table */
#define Shengraw(YINcod) (((int)((YINcod - 0x20a0) & 0x7c)>>(int)2) + 0x01)

/* Yunraw based on PinYin table  */
#define Yunraw(YINcod) 	((int)(((YINcod) - 0x20a0) & 0x7e00) >> (int)9)


/* to see if the char is a start char of a pinyin without sisheng */
#define py0_first_ch(X) (((int)(X)>'A' && (int)(X)<='Z' &&  \
			  (X)!='E' && (X)!='O'&& \
			  (X)!='I' && (X)!='U' && (X)!='V') || \
			  (X)=='a'||(X)=='e'||(X)=='o' || (X)=='n'? 1: 0)

/* to see if the char is a start char of a zhuyin */

    /* for sisheng */
#define S_S_YOMI(X)  	( ((int)(X&0xff)>=0xa1)&&((int)(X&0xff)<=0xbf))||((int)(X>>8)==0x8e)
#define py_first_ch(X)	( py0_first_ch(X) || S_S_YOMI(X) )
#define zy_first_ch(X)   ( ((int)(X&0xff) >= 0xc0 && (int)(X&0xff) <= 0xe9 && \
			    ((int)(X>>(int)8)) == 0x8e) )

#define _cwnn_has_sisheng(YINcod)	( (( (YINcod)& 0x0100) != 0 ) ?1:0)

extern  unsigned char  last_mark;

extern  char  	*py_shengmu_tbl[];  	/* PinYin ShengMu table */
extern  char  	*py_yunmu_tbl[];	/* PinYin YunMu table   */
extern  char  	*zy_shengmu_tbl[];	/* ZhuYin ShengMu table */
extern  char  	*zy_yunmu_tbl[];	/* ZhuYin YunMu table   */

extern  int  	pinyin_tbl[]; 		/* PinYin table		*/
extern  int  	zhuyin_tbl[]; 		/* ZhuYin table 	*/

#endif /* _CPLIB_H_ */
