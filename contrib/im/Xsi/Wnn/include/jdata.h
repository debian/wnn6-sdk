/*
 * $Id: jdata.h,v 2.7.2.1 2000/08/04 05:37:13 kaneda Exp $
 */
/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/

/*	Version 4.0
 */
#ifndef _JDATA_H_
#define _JDATA_H_

/*
 * Hideyuki Kishiba (Jul. 20, 1994)
 * ＦＩ関係情報ヘッダファイルの include
 */
#include "fi_jdata.h"

#ifndef	JS
struct	jdata {
  int	kanji1;
  short	kanji2;			/* */
  short   which ;		/* gyaku henkan? */
  int	serial;			/* index is a serial number of the first 
				 entry which is stored in this entry*/
  int	kosuu;			/* this means the number of elements in this
				 entry */
  int	jishono;		/* jishono in which these entries are stored */
  struct  jdata  *jptr;	/* pointer to another jdata which
				 points out a jdata of the same yomi
				 but (always)different jishono*/
  unsigned short   *hinsi;
#ifdef	CONVERT_with_SiSheng	/* Don't warry. Only use in server and jutil */
/* It's only use CWNN : 四声 */
  unsigned short   *sisheng;
  unsigned short   sisheng_int;
#endif
  UCHAR *hindo;			/* 頻度 */
  UCHAR *hindo_in;		/* 内部頻度 */
  UCHAR *gakusyu;		/* 学習情報 */

  /*
   * Hideyuki Kishiba (Jul. 20, 1994)
   * ＦＩ関係情報検索結果の辞書タイプとポインタを入れる
   */
  int fi_data_type;
  struct fi_dic_data **fi_ddata;
  struct fi_hindo_data **fi_hdata;
};
#endif /* JS */

struct JT {
  unsigned int total;    /* トータル頻度 */
  int gosuu;
  char hpasswd[WNN_PASSWD_LEN];
  int syurui;

  int maxcomment;
  int maxhinsi_list;
  int maxserial;
  int maxtable;			/* For UD Dic */
  int maxhontai;		/* For UD Dic  and Static Dic */
  int maxkanji;
  int maxri1[2];		/* For Rev Dic */
  int maxri2;			/* For Rev Dic, is equal to maxserial */
  

  w_char *comment;
  w_char *hinsi_list;

  UCHAR *hindo;  /* 頻度 */
#ifdef	CONVERT_with_SiSheng		/* Don't warry. Only use in server and jutil */
/* It's only use CWNN : 四声 */
  unsigned short   *sisheng;
  unsigned short   sisheng_int;
#endif
  unsigned short *hinsi;	/* bunpou data is stored here */
  UCHAR *kanji;	/* kanji data is stored here */
  struct uind1 *table; /* anothe tablefile for user jisho */
  UCHAR *hontai;	/*  the tablefile (index-file) */
  struct rind1 *ri1[2];  	/* For Rev Dic */
  struct rind2 *ri2;		/* For Rev Dic */

  short dirty;
  short hdirty;

/* ここから4つは、 登録可能辞書、逆変換可能辞書にのみ関係する */
    int bufsize_kanji;    /* 漢字領域の大きさ */
    int bufsize_serial;   /* 文法、頻度領域の大きさ */
    int bufsize_hontai;     /* インデックス(hontai)の領域の大きさ*/
    int bufsize_table;     /*  インデックス(table)の領域の大きさ*/
    int bufsize_ri1[2];  /* 逆変換可能辞書の table の領域の大きさ  */

  /* hinsi_list wo kakunou suru */
  int maxnode;
  struct wnn_hinsi_node *node;
#ifdef	CONVERT_by_STROKE	/* Don't warry. Only use in server and jutil */
/* Used for Bixing dic  */
    struct b_node *bind;	/* index for yomi <--> kanji search*/
    int		   max_bnode;	/* Max num of tuple     */
    int		   bufsize_bnode;	/* BUFFER size of the b_node */
#endif
  int is_compressed;  /* for compact dictionary */
};

/* for compact dictionary */
/* should be union-ed, but ,, */
#define cd_block_size maxtable
#define cd_data_length maxhontai
#define cd_data_area kanji
#define cd_index hontai
#define cd_data_area_size bufsize_kanji
#define cd_index_size bufsize_hontai
 
struct cd_index_layout { /* for compact dictionary */
    long serial;
    union {
	UCHAR uc4[4];
	long  l;
    } yomi;
};

struct HJT {
  struct wnn_file_uniq dic_file_uniq;
  int maxcomment;
  int maxserial;		/* Max num in dictionary */
  
  int hindo_area;		/* Current num of hindo data area */
  unsigned char *curserial;	/* Current num sets hindo in a area */

  w_char *comment;
  unsigned int **hindo;

  short hdirty;
  short *bufsize_serial;
};


/*
 * structures concerning UD dicts
 */

#define AL_INT(x) ((int)((char *)(x) + 3) & ~0x3)


#define ENDPTR 0  /* The end of (int) pointer list in uind2->next */
#define RD_ENDPTR 0xffffffff  /* The end of (int) pointer list in rind2->next */

struct uind1 {
  int pter1;			/* pointer to uind1 */
  int pter;			/* pinter to uind2 */
  unsigned int yomi1;
  unsigned int yomi2;
};

struct uind2 {
  int next;			/* pointer to uind2 */
  int serial;
  int kanjipter;
  w_char kosuu;
  w_char yomi[1];		/* actually it is variable length */
};

/* MAXTABLE MAX... ha user file no ookisani kuwaete touroku no tame 
   ni totteoku ookisa*/
#define MAXTABLE 100		/* you can touroku MAXTABLE element without 
				 realloc the jisho*/
#define MAXHONTAI MAXTABLE * 4 * 4
#define MAXKANJI MAXTABLE * 10
#define MAXSERIAL MAXTABLE
#define MAXBIND MAXTABLE * 4

/* hindo data area size */
#define MAX_HINDO_DATA_AREA 256

/* 
 * structures concerning REV dicts.
 */

struct rind1 {
  int pter1;			/* pointer to rind1 */
  int pter;			/* pinter to rind2 */
};

struct rind2 {
  int next[2];			/* pointer to rind2 */
  int kanjipter;
};

/*
 * Reverse Dict ni kansite, dotira wo hikuka?
 */

#define D_YOMI 0
#define D_KANJI 1

#define KANJI_str1(pter, which) (((which) == D_YOMI)?((w_char *)((pter) + 2)):\
    ((w_char *)((pter) + 2) + Strlen((w_char *)((pter) + 2)) + 1))

#define KANJI_str(p, which) ((*((p) + 1) & FORWARDED)? \
    KANJI_str1(((*(w_char *)((p) + 2)) << 16 | (*(w_char *)((p) + 4))) \
	       + (p),which): \
    KANJI_str1((p), which))

#define Get_kanji_len(pter, which) (Strlen(KANJI_str(pter, which)))

/* 
 * structures concerning SD dicts.
 */

#define ST_NORMAL 1
#define ST_NOENT 2
#define ST_NOPTER 3
#define ST_SMALL 4
#define ST_TABLE 5

/*
 *Concerning HINSI. But these may not be used.
 */

#define SAKUJO_HINSI 0xfffe /* sakujo sareta tango no hinsi */
#define NANDEMO_EEYO 0xfffd /* nandemo maeni koreru to iu hinsi */

/*
 *Concerning KANJI and COMMENT.
 */

#define HAS_YOMI 1
#define HAS_COMMENT 2
#define FORWARDED 4

/* #define SEPARATE_CHAR '\0'*/
/* Choose character which do not appear as the first byte of w_char */

#define DIC_YOMI_CHAR  1
#define DIC_COMMENT_CHAR  2


#define DIC_HIRAGANA "\\y"
#define DIC_KATAKANA "\\k"

/*#define	DIC_HANKAKU	"\\h"	*//* 半角 *//* 読みのまま */
#define	DIC_ZENKAKU	"\\z"	/* 全角 *//* １２３ */
#define	DIC_NUM_KAN	"\\chan"	/* 漢数字 *//* 一二三 */
#define	DIC_NUM_KANSUUJI "\\chas"	/* 漢数字 *//* 百二十三 */
#define	DIC_NUM_KANOLD	"\\chao"	/* 漢数字 *//* 壱百弐拾参 */
#define	DIC_NUM_HANCAN	"\\nhc"	/* 半角数字 *//* 1,234 */
#define	DIC_NUM_ZENCAN	"\\nzc"	/* 全角数字 *//* １，２３４ */
#define DIC_HEX		"\\X"  /* \Xa4a2 */
#define DIC_HEXc	"\\x"  /* \xa4a2 */
#define DIC_OCT 	"\\0" /* \040   */
#define DIC_ESC 	"\\\\" /* \\ = \  */

#define DIC_OKURI_KIJUN "\\r"
#define DIC_OKURI_FUKUGOU "\\f"
#define DIC_SETTOU_GAKUSYU "\\t"
#define DIC_SETUBI_GAKUSYU "\\b"
#define DIC_HANYOU_GAKUSYU "\\h"
#define DIC_KUGIRI_CHAR '\\'

#ifdef notuse
/* 英数 */
#define	WNN_ALP_HAN	-4	/* 半角 *//* 読みのまま */
#define	WNN_ALP_ZEN	-30	/* 全角 */
/* 記号 */
#define	WNN_KIG_HAN	-5	/* 半角 *//* 読みのまま */
#define	WNN_KIG_JIS	-40	/* 全角(JIS) */
#define	WNN_KIG_ASC	-41	/* 全角(ASC) */
#endif

#ifdef	CONVERT_by_STROKE
struct	b_node{
	short	pter;			/* Point to Indix-2	*/
	short	pter_son;		/* Ptr to first son	*/
	short	pter_next;		/* Ptr to next brother	*/
};

struct	b_koho {
	struct	b_koho	*previou;
	int		value;
	w_char		*p_yomi;
	w_char		*p_kanji;
	unsigned short	*p_hinsi;
	UCHAR		*p_hindo;
	UCHAR		*p_gakusyu;
	int		serial;
	int		dic_no;
};
#endif

#endif /* _JDATA_H_ */
