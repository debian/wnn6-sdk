/*
 * $Id: wnnerror.h,v 2.13.2.1 2000/08/04 05:37:17 kaneda Exp $
 */

/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/

/*	Version 4.0
 */
#ifndef _WNNERROR_H_
#define _WNNERROR_H_

/* file 関係のエラー */
#define WNN_FILE_READ_ERROR	90 /* ファイルを読み込むことができません。 */
#define WNN_FILE_WRITE_ERROR	91 /* ファイルを書き出すことができません。 */
#define WNN_FID_ERROR		92 /* クライアントの読み込んだファイルではありません。 */
#define WNN_NO_MORE_FILE	93 /* これ以上ファイルを読み込むことができません。*/
#define WNN_INCORRECT_PASSWD    94 /* パスワードが間違っています。 */
#define WNN_FILE_IN_USE    	95 /* ファイルが読み込まれています。 */
#define WNN_UNLINK    		96 /* ファイルが削除できません。 */
#define WNN_FILE_CREATE_ERROR	97 /* ファイルが作成出来ません。*/
#define WNN_NOT_A_FILE		98	/*Ｗｎｎのファイルでありません。*/
#define WNN_INODE_CHECK_ERROR   99 /* ファイルのI-nodeとFILE_UNIQを一致させる事ができません。 */

/*	V3	*/
/* 辞書追加関係のエラー */
#define WNN_NO_EXIST		1	/*ファイルが存在しません。*/
#define WNN_MALLOC_ERR		3	/* メモリを確保できません。 */
#define WNN_NOT_A_DICT		5 	/*辞書ではありません。*/
#define WNN_NOT_A_HINDO_FILE	6	/*頻度ファイルではありません。*/
#define WNN_NOT_A_FUZOKUGO_FILE	7	/*付属語ファイルではありません。*/
#define WNN_JISHOTABLE_FULL	9	/*辞書テーブルが一杯です。*/
#define WNN_HINDO_NO_MATCH	10	/*頻度ファイルが、指定された辞書の
					  頻度ファイルではありません。*/
#define WNN_OPENF_ERR		16	/*ファイルがオープンできません。*/
#define WNN_NOT_HINDO_FILE	17	/*正しい頻度ファイルでありません。*/
#define WNN_NOT_FZK_FILE	18	/*正しい付属語ファイルでありません。*/
#define WNN_FZK_TOO_DEF		19	/*付属語の個数、ベクタ長さなどが
					  多過ぎます*/
/* 辞書削除関係のエラー */
#define WNN_DICT_NOT_USED	20	/*その番号の辞書は、使われていません。*/

/* 変換時のエラー */
#define WNN_BAD_FZK_FILE  24      /*付属語ファイルの内容が正しくありません*/
#define WNN_GIJI_HINSI_ERR	25	/*疑似品詞番号が異常です
					  hinsi.dataが正しくありません*/
#define WNN_NO_DFE_HINSI	26	/*未定義の品詞が前端品詞として
					  定義されています Not Used*/
#define WNN_FZK_FILE_NO_LOAD	27	/*付属語ファイルが読み込まれていません*/

/* jishobiki.c */
#define WNN_DIC_ENTRY_FULL	30	/*辞書のエイントリが多過ぎます。*/
#define WNN_LONG_MOJIRETSU	31	/*変換しようとする文字列が長過ぎます。*/
#define WNN_WKAREA_FULL		32	/*付属語解析領域が不足しています。*/
#define WNN_JKTAREA_FULL 	34	/* 候補が多過ぎて次候補が取り出せません。 */
#define WNN_NO_KOUHO		35	/*候補が 1 つも作れませんでした*/

/* 単語登録時のエラー */
#define WNN_YOMI_LONG		40	/*読みが長過ぎます。*/
#define WNN_KANJI_LONG		41	/*漢字が長過ぎます。*/
#define WNN_NOT_A_UD		42 /*指定された辞書は、登録可能ではありません。*/
#define WNN_NO_YOMI		43	/*読みの長さが0です。*/
#define WNN_NOT_A_REV		44 /*指定された辞書は、逆引き可能ではありません。*/
#define WNN_RDONLY		45	/*リードオンリーの辞書に登録しようとしました。*/
#define WNN_DICT_NOT_IN_ENV	46	/*環境に存在しない辞書に登録しようとしました。*/

/* 頻度更新のエラー */
#define WNN_RDONLY_HINDO	49 /* リードオンリーの頻度を変更しようとしました。 */

/* 単語削除時、品詞削除時のエラー */
/*
WNN_RDONLY
*/
#define WNN_WORD_NO_EXIST	50	/*指定された単語が存在しません。*/

/* 初期化の時のエラー */
#define WNN_MALLOC_INITIALIZE	60	/* メモリを確保できません。 */

/*
 * Errors are supported from 4.004
 */
#define WNN_ACCESS_DENIED	61
#define WNN_NOT_SUPPORT_PACKET	62
#define WNN_NOT_SUPPORT_EXTENSION	63
#define WNN_ACCESS_NO_HOST	64
#define WNN_ACCESS_NO_USER	65
#define WNN_NO_JSERVER		66

#define WNN_SOME_ERROR		68	/* 何かのエラーが起こりました。*/
#define WNN_SONOTA		69	/*バグが発生している模様です。Not Used*/
#define WNN_JSERVER_DEAD	70	/*サーバが動作していません。(jlib)*/
#define WNN_ALLOC_FAIL		71	/*メモリを確保できません(jlib)*/
#define WNN_SOCK_OPEN_FAIL	72	/*jd_beginでsocketのopenに失敗(jlib)*/
#define WNN_BAD_VERSION		73	/*通信プロトコルのバージョンが合っていません。*/
#define WNN_BAD_ENV		74      /*クライアントの生成した環境ではありません。*/

#define WNN_MKDIR_FAIL		80	/* ディレクトリを作り損なった */


/*品詞ファイル */
#define WNN_TOO_BIG_HINSI_FILE 100 	/*品詞ファイルが大き過ぎます。*/
#define WNN_TOO_LONG_HINSI_FILE_LINE 101		/*品詞ファイルが大き過ぎます。*/
#define WNN_NO_HINSI_DATA_FILE 102	/*品詞ファイルが存在しません。 */
#define WNN_BAD_HINSI_FILE 103	       /*品詞ファイルの内容が間違っています。*/

#define WNN_HINSI_NOT_LOADED 105 	/* 品詞ファイルが読み込まれていません。*/
#define WNN_BAD_HINSI_NAME 106		/* 品詞名が間違っています */
#define WNN_BAD_HINSI_NO 107		/* 品詞番号が間違っています */

#define NOT_SUPPORTED_OPERATION 109 /*その操作はサポートされていませんNot Used*/

/* 
 *
 *  jl （高水準ライブラリ）のエラー
 * 
 */

#define WNN_CANT_OPEN_PASSWD_FILE 110  /* パスワードの入っているファイルが
					 オープンできません(jl) */
/* 初期化時のエラー  */
#define WNN_RC_FILE_NO_EXIST 111 /* uumrcファイルが存在しません(jl) Not Used*/
#define WNN_RC_FILE_BAD_FORMAT 112 /* uumrcファイルの形式が誤っています(jl)Not Used */
#define WNN_NO_MORE_ENVS  113 /* これ以上環境を作ることは出来ません。*/
#define WNN_FILE_NOT_READ_FROM_CLIENT 114 /* このクライアントが読み込んだファイルでありません。*/
#define WNN_NO_HINDO_FILE 115 /* 辞書に頻度ファイルがついていません。*/

#define WNN_CANT_CREATE_PASSWD_FILE 116 /*パスワードのファイルが作成出来ません。*/

/*
 * Errors are supported from 4.F00
 */
#define WNN_TEMPORARY_ALREADY_ADD 500 /* The temporary dic is already added */
#define WNN_TEMPORARY_ADD_FAILED 501 /* Addition of temporary dic failed */
#define WNN_TEMPORARY_DELETE_FAILED 502 /* Deletion of temporary dic failed */


/*
 *      WNNDS
 */

/* Errors about connection */
#define WNN_DS_DEAD		1001	/* WNNDS is died */
#define WNN_DS_BAD_VERSION	1002	/* dserver denied access */
#define WNN_DS_ACCESS_DENIED	1003	/* dserver denied access */

/*
 *	WNNDM
 */
#define WNN_DM_DEAD		2001     /* WNNDM is died */
#define WNN_DM_BAD_VERSION	2002

/*
 * Hideyuki Kishiba (Jul. 8, 1994)
 * New error number for FI-Wnn
 */
/* 辞書追加関係のエラー */
#define WNN_NOT_A_FI_DICT	3001	/* ＦＩ関係辞書ではありません */
#define WNN_NOT_A_FI_HINDO	3002	/* ＦＩ関係辞書ではありません */
#define WNN_NOT_A_FI_SYSTEM	3003	/* 指定された辞書は、ＦＩ関係システム辞書ではありません */
#define WNN_NOT_A_FI_USER	3004	/* 指定された辞書は、ＦＩ関係ユーザ辞書ではありません */
#define WNN_USE_OLD_FZK		3005	/* 古いバージョンの付属語ファイルが設定されています */
#define NOT_SHARE_HINDO_FILE	3006	/* １つの頻度ファイルを異なる辞書間で共用しようとしました */

/* offline 学習時のエラー */
#define WNN_LOCKED		3010 	/* サーバーがロックされています */
/* license server */
#define WNN_NOT_LICENSE		3011	/* ライセンスが取得できませんでした */
#endif /* _WNNERROR_H_ */
/*
  Local Variables:
  kanji-flag: t
  End:
*/
