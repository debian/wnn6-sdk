/*
 * $Id: jd_sock.h,v 2.17.2.1 2000/08/04 05:37:13 kaneda Exp $
 */
/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/


/*	Version 4.0
 */
/*	jd_sock.h
	jslib header file
*/

#ifdef UX386
#undef	AF_UNIX
#include <X11/Xos.h>
#else
#include <sys/types.h>
#endif /* UX386 */
#include <sys/socket.h>
#ifdef	AF_UNIX
#include <sys/un.h>
#endif	/* AF_UNIX */

#ifdef UX386
#include <net/in.h>
#else
#include <netinet/in.h>
#endif /* UX386 */

#if	defined(uniosu) || defined(UX386)
#include <net/netdb.h>	/* SX */
#else
#include <netdb.h>	/* SUN or BSD SYSV*/
#endif /*uniosu */

#ifdef TAIWANESE
#ifndef CHINESE
#define CHINESE
#endif
#endif

#define	ERROR	-1

# define WNN_PORT_IN_ORIG	(0x5701)
# define WNN_DSPORT_IN_ORIG  	(0x6660)
# define WNN_JJPORT_OFFSET	(4191)
# define WNN_DMPORT_IN		(0x6860)
# define DMSERVERNAME		"wnn6_DM"

#ifdef	JAPANESE	/* Japanese */
# define WNN_PORT_IN	WNN_PORT_IN_ORIG
# define WNN_DSPORT_IN  WNN_DSPORT_IN_ORIG
# define UNIX_SOCKET_NAME	"/tmp/jd_sockV6"	/* for jserver */
# define LANG_NAME	"ja_JP"
# define SERVERNAME	"wnn6"
# define DSSERVERNAME   "wnn6_DS"
# define MESSAGE_FILE	"jserver.msg"
#else	/* JAPANESE */

#ifdef	CHINESE
#ifdef	TAIWANESE		/* Traditional Chinese */
# define WNN_PORT_IN	WNN_PORT_IN_ORIG + 0x30
# define WNN_DSPORT_IN	WNN_DSPORT_IN_ORIG + 0x30
# define UNIX_SOCKET_NAME	"/tmp/td_sockV6"	/* for tserver */
# define LANG_NAME	"zh_TW"
# define SERVERNAME	"wnn6_Tw"
# define DSSERVERNAME   "wnn6_TwDS"
# define MESSAGE_FILE	"tserver.msg"
#else	/* TAIWANESE */		/* Simplified Chinese */
# define WNN_PORT_IN	WNN_PORT_IN_ORIG + 0x10
# define WNN_DSPORT_IN  WNN_DSPORT_IN_ORIG + 0x10
# define UNIX_SOCKET_NAME	"/tmp/cd_sockV6"	/* for cserver */
# define LANG_NAME	"zh_CN"
# define SERVERNAME	"wnn6_Cn"
# define DSSERVERNAME   "wnn6_CnDS"
# define MESSAGE_FILE	"cserver.msg"
#endif	/* TAIWANESE */
#else	/* CHINESE */

#ifdef	KOREAN			/* Korean */
# define WNN_PORT_IN	WNN_PORT_IN_ORIG + 0x20
# define WNN_DSPORT_IN  WNN_DSPORT_IN_ORIG + 0x20
# define UNIX_SOCKET_NAME	"/tmp/kd_sockV6"	/* for kserver */
# define LANG_NAME	"ko_KR"
# define SERVERNAME	"wnn6_Kr"
# define DSSERVERNAME   "wnn6_KrDS"
# define MESSAGE_FILE	"kserver.msg"
#else	/* KOREAN */

# define WNN_PORT_IN	WNN_PORT_IN_ORIG
# define WNN_DSPORT_IN  WNN_DSPORT_IN_ORIG
# define UNIX_SOCKET_NAME	"/tmp/jd_sockV6"	/* for jserver */
# define LANG_NAME	"ja_JP"
# define SERVERNAME	"wnn6"
# define DSSERVERNAME   "wnn6_DS"
# define MESSAGE_FILE	"jserver.msg"
#endif	/* KOREAN */
#endif	/* CHINESE */
#endif	/* JAPANESE */

#define	S_BUF_SIZ	1024	/* NEVER change this */
#define	R_BUF_SIZ	1024	/* NEVER change this */

/* usually defined in <netinet/in.h> */
#ifndef INADDR_NONE
#define INADDR_NONE	0xffffffff
#endif 
