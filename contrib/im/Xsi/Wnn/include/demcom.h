/*
 * $Id: demcom.h,v 2.21.2.1 2000/08/04 05:37:12 kaneda Exp $
 */

/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/

/*	Version 4.0
 */
/*
	demcom.h
	entry functions	definitions
*/
#ifndef _DEMCOM_H_
#define _DEMCOM_H_

#define JLIB_VERSION    0x4F00	/* For FI-Wnn */

/*
 *	Demon Commands
 */
#define	JS_VERSION	0x00
#define	JS_OPEN		0x01
/*	#define	JS_OPEN_IN	0x02	*/
#define	JS_CLOSE	0x03
#define	JS_CONNECT	0x05
#define	JS_DISCONNECT	0x06
#define	JS_ENV_EXIST	0x07
#define	JS_ENV_STICKY	0x08
#define	JS_ENV_UN_STICKY	0x09


#define	JS_KANREN	0x11
#define	JS_KANTAN_SHO	0x12
#define	JS_KANZEN_SHO	0x13
#define	JS_KANTAN_DAI	0x14
#define	JS_KANZEN_DAI	0x15
#define	JS_HINDO_SET	0x18


#define	JS_DIC_ADD	0x21
#define	JS_DIC_DELETE	0x22
#define	JS_DIC_USE	0x23
#define	JS_DIC_LIST	0x24
#define	JS_DIC_INFO	0x25

#define	JS_FUZOKUGO_SET	0x29
#define	JS_FUZOKUGO_GET	0x30


#define	JS_WORD_ADD	0x31
#define	JS_WORD_DELETE	0x32
#define	JS_WORD_SEARCH	0x33
#define	JS_WORD_SEARCH_BY_ENV	0x34
#define	JS_WORD_INFO	0x35
#define JS_WORD_COMMENT_SET 0x36

#define	JS_PARAM_SET	0x41
#define	JS_PARAM_GET	0x42

#define	JS_MKDIR	0x51
#define	JS_ACCESS	0x52
#define	JS_WHO		0x53
#define	JS_ENV_LIST	0x55
#define	JS_FILE_LIST_ALL	0x56
#define	JS_DIC_LIST_ALL	0x57

#define	JS_FILE_READ	0x61
#define	JS_FILE_WRITE	0x62
#define	JS_FILE_SEND	0x63
#define	JS_FILE_RECEIVE	0x64

#define	JS_HINDO_FILE_CREATE	0x65
#define	JS_DIC_FILE_CREATE	0x66
#define JS_FILE_REMOVE	0x67

#define	JS_FILE_LIST	0x68
#define	JS_FILE_INFO	0x69
#define	JS_FILE_LOADED	0x6A
#define	JS_FILE_LOADED_LOCAL	0x6B
#define	JS_FILE_DISCARD	0x6C
#define JS_FILE_COMMENT_SET 0x6D
#define JS_FILE_PASSWORD_SET 0x6E /* 89/9/8 */

#define	JS_FILE_STAT	0x6F
#define JS_KILL		0x70

#define	JS_HINDO_FILE_CREATE_CLIENT	0x71
#define	JS_HINSI_LIST			0x72
#define JS_HINSI_NAME	0x73
#define JS_HINSI_NUMBER	0x74
#define JS_HINSI_DICTS  0x75
#define JS_HINSI_TABLE_SET 0x76

/*
 * Hideyuki Kishiba (Jul. 8, 1994)
 * Start packets are added from 4.F00(FI-Wnn).
 * All packets have 0xF00000 as offset.
 */
#define JS_VERSION_OFFSET_OF_FIWNN	0xF00000


/* Access Control packets */
#define JS_ACCESS_ADD_HOST		(JS_VERSION_OFFSET_OF_FIWNN|0x11)
#define JS_ACCESS_ADD_USER		(JS_VERSION_OFFSET_OF_FIWNN|0x12)
#define JS_ACCESS_REMOVE_HOST		(JS_VERSION_OFFSET_OF_FIWNN|0x13)
#define JS_ACCESS_REMOVE_USER		(JS_VERSION_OFFSET_OF_FIWNN|0x14)
#define JS_ACCESS_ENABLE		(JS_VERSION_OFFSET_OF_FIWNN|0x15)
#define JS_ACCESS_DISABLE		(JS_VERSION_OFFSET_OF_FIWNN|0x16)
#define JS_ACCESS_GET_INFO		(JS_VERSION_OFFSET_OF_FIWNN|0x17)

/* Temprary registration for Katakana and BunsetsuGiri packet */
#define JS_TEMPORARY_DIC_ADD		(JS_VERSION_OFFSET_OF_FIWNN|0x21)
#define JS_TEMPORARY_DIC_DELETE		(JS_VERSION_OFFSET_OF_FIWNN|0x22)
#define JS_AUTOLEARNING_WORD_ADD	(JS_VERSION_OFFSET_OF_FIWNN|0x23)
#define JS_SET_AUTOLEARNING_DIC		(JS_VERSION_OFFSET_OF_FIWNN|0x24)
#define JS_GET_AUTOLEARNING_DIC		(JS_VERSION_OFFSET_OF_FIWNN|0x25)
#define JS_IS_LOADED_TEMPORARY_DIC	(JS_VERSION_OFFSET_OF_FIWNN|0x26)
#define JS_TEMPORARY_WORD_ADD		(JS_VERSION_OFFSET_OF_FIWNN|0x27)

/* For Wnn5 */
#define JS_SET_HENKAN_ENV		(JS_VERSION_OFFSET_OF_FIWNN|0x31)
#define JS_GET_HENKAN_ENV		(JS_VERSION_OFFSET_OF_FIWNN|0x32)
#define JS_SET_HENKAN_HINSI		(JS_VERSION_OFFSET_OF_FIWNN|0x33)
#define JS_GET_HENKAN_HINSI		(JS_VERSION_OFFSET_OF_FIWNN|0x34)
#define JS_HENKAN_WITH_DATA		(JS_VERSION_OFFSET_OF_FIWNN|0x35)



/* For FI-Wnn */
#define JS_FI_DIC_ADD                   (JS_VERSION_OFFSET_OF_FIWNN|0x61)
#define JS_FI_HINDO_FILE_CREATE         (JS_VERSION_OFFSET_OF_FIWNN|0x62)
#define JS_FI_HINDO_FILE_CREATE_CLIENT  (JS_VERSION_OFFSET_OF_FIWNN|0x63)
#define JS_DIC_FILE_CREATE_CLIENT   	(JS_VERSION_OFFSET_OF_FIWNN|0x64)
#define JS_FI_KANREN		   	(JS_VERSION_OFFSET_OF_FIWNN|0x65)
#define JS_SET_FI_PRIORITY		(JS_VERSION_OFFSET_OF_FIWNN|0x66)
#define JS_OPTIMIZE_FI			(JS_VERSION_OFFSET_OF_FIWNN|0x67)

/* For FI-Wnn ikeiji */
#define JS_HENKAN_IKEIJI                (JS_VERSION_OFFSET_OF_FIWNN|0x6f)

/* For wnnoffline */
#define JS_LOCK				(JS_VERSION_OFFSET_OF_FIWNN|0x71)
#define JS_UNLOCK			(JS_VERSION_OFFSET_OF_FIWNN|0x72)

/* For dictutil */
#define JS_FI_DIC_LIST			(JS_VERSION_OFFSET_OF_FIWNN|0x81)
#define JS_FI_DIC_LIST_ALL		(JS_VERSION_OFFSET_OF_FIWNN|0x82)
#define JS_FUZOKUGO_LIST		(JS_VERSION_OFFSET_OF_FIWNN|0x83)


#endif /* _DEMCOM_H_ */
