/*
 * $Id: config.h,v 2.29.2.1 2000/08/04 05:37:11 kaneda Exp $
 */
/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/

/*	Version 4.0
 */
#ifndef _CONFIG_H_
#define _CONFIG_H_

#ifndef	LIBDIR
#define	LIBDIR		"/usr/local/lib/wnn6"
#endif	/* LIBDIR */

#ifdef TAIWANESE
#ifndef CHINESE
#define CHINESE
#endif
#endif

#define WNN_USERNAME_ENV	"WNNUSER"
#define WNN_JSERVER_ENV		"JSERVER"
#define WNN_CSERVER_ENV		"CSERVER"
#define WNN_KSERVER_ENV		"KSERVER"
#define WNN_TSERVER_ENV		"TSERVER"
#define WNN_J_LANG		"ja_JP"
#define WNN_C_LANG		"zh_CN"
#define WNN_K_LANG		"ko_KR"
#define WNN_T_LANG		"zh_TW"
#ifdef JAPANESE
#define WNN_DEFAULT_LANG	WNN_J_LANG
#define WNN_DEF_SERVER_ENV	WNN_JSERVER_ENV
#else /* JAPANESE */
#ifdef CHINESE
#ifdef TAIWANESE
#define WNN_DEFAULT_LANG	WNN_T_LANG
#define WNN_DEF_SERVER_ENV	WNN_TSERVER_ENV
#else /* TAIWANESE */
#define WNN_DEFAULT_LANG	WNN_C_LANG
#define WNN_DEF_SERVER_ENV	WNN_CSERVER_ENV
#endif /* TAIWANESE */
#else /* CHINESE */
#ifdef KOREAN
#define WNN_DEFAULT_LANG	WNN_K_LANG
#define WNN_DEF_SERVER_ENV	WNN_KSERVER_ENV
#else /* KOREAN */
#define WNN_DEFAULT_LANG	WNN_J_LANG
#define WNN_DEF_SERVER_ENV	WNN_JSERVER_ENV
#endif /* KOREAN */
#endif /* CHINESE */
#endif /* JAPANESE */
#define WNN_UUM_ENV  		"UUMRC"
#define WNN_KEYBOARD_ENV	"KEYBOARD"
#define WNN_COUNTDOWN_ENV	"UUM_COUNTDOWN"

#define PATHNAMELEN	256

/* for jserver */
#ifndef	SERVER_INIT_FILE
# ifdef JAPANESE
#  define SERVER_INIT_FILE	"/ja_JP/jserverrc"
# else /* JAPANESE */
# ifdef	CHINESE
# ifdef	TAIWANESE
#  define SERVER_INIT_FILE	"/zh_TW/tserverrc"
# else /* TAIWANESE */
#  define SERVER_INIT_FILE	"/zh_CN/cserverrc"
# endif /* TAIWANESE */
# else /* CHINESE */
# ifdef KOREAN
#  define SERVER_INIT_FILE	"/ko_KR/kserverrc"
# else /* KOREAN */
#  define SERVER_INIT_FILE	"/ja_JP/jserverrc"	/* Default */
# endif /* KOREAN */
# endif /* CHINESE */
# endif /* JAPANESE */
#endif
#define JSERVER_DIR		"/usr/local/lib/dic"
#define WNN_ACCESS_FILE		"wnnhosts"

/* for uum */
#define RCFILE			"/uumrc"	/* LIBDIR/@LANG/RCFILE */
#define USR_UUMRC		"/.uumrc"
#define RKFILE			"/rk/mode"	/* LIBDIR/@LANG/RKFILE */
#define CPFILE			"/uumkey"	/* LIBDIR/@LANG/CPFILE */
#define MESSAGEFILE		"/message_file"


#define RKFILE_ATOK8		"/rk/mode.atok8"
#define CPFILE_ATOK8		"/uumkey.atok8"
#define RKFILE_EGBRIDGE		"/rk/mode.egbridge"
#define CPFILE_EGBRIDGE		"/uumkey.egbridge"
#define RKFILE_WNN4		"/rk/mode.wnn4"
#define CPFILE_WNN4		"/uumkey.wnn4"
#define RKFILE_WNN6		"/rk/mode.eitango"
#define CPFILE_WNN6		"/uumkey.wnn6"
#ifdef ATOK7
#define RKFILE_ATOK7		"/rk/mode.atok7"
#define CPFILE_ATOK7		"/uumkey.atok7"
#endif /* ATOK7 */
#ifdef VJEDELTA
#define RKFILE_VJEDELTA		"/rk/mode.vjedelta"
#define CPFILE_VJEDELTA		"/uumkey.vjedelta"
#endif /* VJEDELTA */
#ifdef WXII
#define RKFILE_WXII		"/rk/mode.wxii"
#define CPFILE_WXII		"/uumkey.wxii"
#endif /* WXII */
#ifdef MSIME
#define RKFILE_MSIME		"/rk/mode.msime"
#define CPFILE_MSIME		"/uumkey.msime"
#endif /* MSIME */

#define CONVERT_FILENAME	"/cvt_key_tbl"

#define ENVRCFILE		"/wnnenvrc"
#ifndef	HINSIDATA_FILE
# define HINSIDATA_FILE		"/ja_JP/hinsi.data"
#endif	/* HINSIDATA_FILE */

#define USR_DIC_DIR_VAR "@USR"

#define FIWNN_DIR "/.Wnn6"	/* Path under the home directory of user */
#define DEFAULT_RC_NAME "@DEFAULT"


/*
  if you wish to do flow control active for your tty,
  define FLOW_CONTROL to 1.
  note that this 'tty' means the tty from which wnn is invoked.
 */

#define FLOW_CONTROL 0

#define C_LOCAL '!'		
/* For Local File Name.
   Local File Name is send as "Hostname!Filename" when C_LOCAL is '!'.
   It is also used in jl_library to specify local file-name, that is,
   file-names which start with this character are considered to be local.
   */


/*
  define default kanji code system for your 'tty' side and 'pty' side.
  'tty' side (TTY_KCODE) means 'your terminal's code'.
  'pty' side (PTY_KCODE) means 'application's code'.
 */
#define TTY_KCODE J_EUJIS
#define PTY_KCODE J_EUJIS

#define TTY_CCODE C_EUGB
#define PTY_CCODE C_EUGB
#define TTY_TCODE C_BIG5
#define PTY_TCODE C_BIG5

#define TTY_HCODE K_EUKSC
#define PTY_HCODE K_EUKSC

/*
  OPTIONS defines what options are available.
  define it by modifying ALL_OPTIONS string.
  if you wish to make some option abailable, leave that character unchanged.
  else turn that character some non-option character, ex. '*'.
  TAKE CARE NOT TO MOVE CHARACTER POSITION, ORDER, ETC!!

  see sdefine.h for precise definition of ALL_OPTIONS. defines below
  may be incorrect.

#define GETOPTSTR   "hHujsUJSPxXk:c:r:l:D:n:vL:"
#define ALL_OPTIONS "hHujsUJSPxXkcrlDnvL"

#ifndef OPTIONS
#define OPTIONS ALL_OPTIONS
#endif
 */

#define	WNN_TIMEOUT	5	/* connect の際に５秒待つんだよ */
#define	WNN_DISP_MODE_LEN	5	/* モード表示に必要なcolumn数 */

#endif /* _CONFIG_H_ */
