/*
 * $Id: jslib.h,v 2.40.2.1 2000/08/04 05:37:15 kaneda Exp $
 */

/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/

/*	Version 4.0
 */
/*
	Nihongo	Henkan	Library Header File
*/
#ifndef	_JSLIB_H_
#define _JSLIB_H_

#ifndef	_WNN_SETJMP
#define	_WNN_SETJMP
#include <setjmp.h>
#endif

#ifndef	w_char
#define	w_char	unsigned short
#endif	/* w_char */

/* 前端ベクタ(品詞番号)の定義 */
#define	WNN_BUN_SENTOU	-1	/* 大文節の先頭になれる奴 */
#define	WNN_ALL_HINSI	-2	/* なんでもええよ */
/* 終端ベクタの定義 */
#define	WNN_VECT_KANREN	0	/* 連文節変換の終端ベクター */
#define	WNN_VECT_KANTAN	1	/* 単文節変換の終端ベクター */
#define	WNN_VECT_KANZEN	1	/* 全候補取出しの終端ベクター 
				   単文節変換時のものと等しい。*/ 
#define WNN_VECT_BUNSETSU 2     /* 連文節変換時の、各文節の終端ベクター*/
#define	WNN_VECT_NO	-1	/* 終端ベクタ1 無し */
/* 文節の終端の接続情報 */
#define	WNN_CONNECT_BK	1	/* 指定された終端ベクターに接続できた。*/
#define	WNN_NOT_CONNECT_BK	0	/* */
/* 文節の前端の接続情報 */
#define	WNN_CONNECT	1	/* 指定された(品詞、付属語)に接続できた。*/
#define	WNN_SENTOU	2	/* 大文節の先頭 */
#define	WNN_NOT_CONNECT	3	/* 先頭になれないし、前に接続できない */
#define	WNN_GIJI	4	/* 疑似文節を作った。*/

/* 疑似文節の直前に選択した候補 */
#define WNN_HIRAGANA	-1	/* ひらがな *//* 読みのまま */
#define WNN_KATAKANA	-11	/* カタカナ */
#define WNN_IKEIJI_ENTRY	-50	/* 異形字 */
/* 数字 */
#define	WNN_NUM_HAN	-2	/* 半角数字 *//* 読みのまま */
#define	WNN_NUM_ZEN	-12	/* 全角数字 *//* １２３ */
#define	WNN_NUM_KAN	-13	/* 漢数字 *//* 一二三 */
#define	WNN_NUM_KANSUUJI -15	/* 漢数字 *//* 百二十三 */
#define	WNN_NUM_KANOLD	-16	/* 漢数字 *//* 壱百弐拾参 */
#define	WNN_NUM_HANCAN	-17	/* 半角数字 *//* 1,234 */
#define	WNN_NUM_ZENCAN	-18	/* 全角数字 *//* １，２３４ */
/* 英数 */
#define	WNN_ALP_HAN	-4	/* 半角 *//* 読みのまま */
#define	WNN_ALP_ZEN	-30	/* 全角 */
/* 記号 */
#define	WNN_KIG_HAN	-5	/* 半角 *//* 読みのまま */
#define	WNN_KIG_JIS	-40	/* 全角(JIS) */
#define	WNN_KIG_ASC	-41	/* 全角(ASC) */
/* 送り基準 */
#define WNN_OKURI_REGULATION -1	/* 本則 */
#define WNN_OKURI_NO 	0	/* 送らない */
#define	WNN_OKURI_YES	1	/* 送る */
/* 変換候補 */
#define WNN_KANA_KOUHO	0	/* ひらがな */
#define WNN_KANJI_KOUHO	1	/* 漢字 */
/* 変換種別 */
#define WNN_KANREN 0
#define WNN_KANTAN_DAI 1
#define WNN_KANZEN_DAI 2
#define WNN_KANTAN_SHO 3
#define WNN_KANZEN_SHO 4

/* 異形字 変換種別 */
#define WNN_IKEIJI_DAI (WNN_KANZEN_SHO+1)

#define WNN_FT_DICT_FILE 1
#define WNN_FT_HINDO_FILE 2
#define WNN_FT_FUZOKUGO_FILE 3
/*
 * Hideyuki Kishiba (Jul. 8, 1994)
 * New file type for FI-Wnn
 */
#define WNN_FT_FI_DICT_FILE 4
#define WNN_FT_FI_HINDO_FILE 5

#define WNN_STATIC_DICT 1		/* static dict */
#define WNN_UD_DICT 2			/* updatable dict */
#define WNN_REV_DICT 3		/* Updatable Dictonary with Reverse Index */
#define WNN_REG_DICT 4		/* Regular Expressino Dictonary */
/*
 * Hideyuki Kishiba (Jul. 8, 1994)
 * New dictionary type for FI-Wnn
 */
#define WNN_COMPACT_DICT 	5	/* コンパクト辞書 */
#define WNN_FI_SYSTEM_DICT 	6	/* ＦＩ関係システム辞書 */
#define WNN_FI_USER_DICT 	7	/* ＦＩ関係ユーザ辞書 */
#define WNN_FI_HINDO_FILE 	8	/* ＦＩ関係頻度 */
#define WNN_GROUP_DICT		9	/* グループ辞書 */
#define	WNN_MERGE_DICT		10	/* マージ辞書 */

#define WNN_SYSTEM_DICT		0x80000000

/*
 * Seiji KUWARI (Dec. 14, 1994)
 * Numbers of special dictionaries
 */ 
#define WNN_MAEKAKUTEI_DIC	-2
#define WNN_MUHENKAN_DIC	-3
#define WNN_BUNSETSUGIRI_DIC	-4

/* for CHINESE PIN-IN : with Si Sheng (四声) */
#define CWNN_REV_DICT		0x103	/* Updatable with Reverse Index */

/* for CHINESE: Bi Xing (筆形) */
#define BWNN_REV_DICT		0x203	/* Updatable with Reverse Index */

#define WNN_DIC_MERGE		4	/* for merge dic */
#define WNN_DIC_GROUP		3	/* for group dic */
#define	WNN_DIC_TEMPORARY	2
#define	WNN_DIC_RDONLY	        1
#define	WNN_DIC_RW	        0

#define	WNN_DIC_ADD_REV 1	/* 逆変換 */
#define	WNN_DIC_ADD_NOR 0	/* 通常の変換 */

/* header file for dic_syurui */
#define WNN_FILE_NOT_EXIST -1
#define WNN_NOT_A_JISHO 0
/* KURI
#define WNN_USER_DIC 1
#define WNN_SYSTEM_DIC 3
*/
#define WNN_HINDO_FILE 4
#define WNN_FUZOKUGO_FILE 5

/* set_hindo operation */
#define WNN_HINDO_NOP -2
#define WNN_IMA_ON -3
#define WNN_IMA_OFF -4
#define WNN_HINDO_INC -3
#define WNN_HINDO_DECL -4
#define WNN_ENTRY_NO_USE -1

/* set hindo probability */
#define WNN_HINDO_NOT    0
#define WNN_HINDO_ALWAYS 1
#define WNN_HINDO_HIGH   2
#define WNN_HINDO_NORMAL 3
#define WNN_HINDO_LOW    4

/* Auto learning operation */
#define WNN_MUHENKAN_LEARNING		1
#define WNN_BUNSETSUGIRI_LEARNING	2

#define WNN_NO_LEARNING		-1
#define WNN_TEMPORARY_LEARNING	-2

/*	Wnn constant
*/
#define WNN_HOSTLEN 16
#define WNN_ENVNAME_LEN 32

/*	js_who	*/
#define WNN_MAX_ENV_OF_A_CLIENT 32

struct wnn_jwho {
	int sd;   /* jserver 内のソケットディスクリプタ*/
	char user_name[64];   /* ユーザ名 */
	char host_name[64];   /* ホスト名 */
	int  env[WNN_MAX_ENV_OF_A_CLIENT];  /* このクライアントが使用している
				環境番号の列。空いているところには、-1 が入る */
};
typedef struct wnn_jwho WNN_JWHO;

/*	js_env_list	*/
#define WNN_MAX_JISHO_OF_AN_ENV 30
#define WNN_MAX_FILE_OF_AN_ENV 60

 struct wnn_env_info {
        int	env_id;		/* 環境番号 */
	char	env_name[WNN_ENVNAME_LEN]; /* 環境名 */
	int	ref_count;	/* 参照数 */
	/* struct wnn_param; */
	int	fzk_fid;	/* 付属語のファイル番号 */
	int jishomax;		/* 使用している辞書の個数 */
	int	jisho[WNN_MAX_JISHO_OF_AN_ENV];	/* 使用している辞書の辞書番号。
					   最初のjishomax個だけ意味がある */
	int	file[WNN_MAX_FILE_OF_AN_ENV]; /* この環境につながっている
				 ファイル番号(空いているところには、-1 が入る) */
};
typedef struct wnn_env_info WNN_ENV_INFO;

#define WNN_COMMENT_LEN 512     /* jisho no comment no nagasa */
#define WNN_F_NAMELEN 256
#define WNN_PASSWD_LEN 16  /* File Passwd Length */


struct wnn_dic_info {
	int dic_no;		/* 辞書番号 */
	int body;		/* 辞書本体のファイル番号 */
	int hindo;		/* 頻度のファイル番号 */
	int rw;			/* 辞書が登録可能かどうか
				   (WNN_DIC_RW, WNN_DIC_RDONLY) */
	int hindo_rw;		/* 頻度が更新可能かどうか
				   (WNN_DIC_RW, WNN_DIC_RDONLY) */
	int enablef;		/* 辞書が使用中かどうか
				   (1 = 使用中, 0 = 使用中断) */
	int nice;		/* 辞書の変換時の優先度 */
	int rev;		/* 逆変換か、どうか(1 = 逆変換, 0 = 正変換) */
/* added H.T */
	w_char comment[WNN_COMMENT_LEN];	/* 辞書のコメント */
	char fname[WNN_F_NAMELEN]; /* 辞書のファイル名 */
	char hfname[WNN_F_NAMELEN]; /* 頻度のファイル名 */
	char passwd[WNN_PASSWD_LEN]; /* 辞書のパスワード */
	char hpasswd[WNN_PASSWD_LEN]; /* 頻度のパスワード */
	int type;		/* 辞書の種類(WNN_UD_DICT,WNN_STATIC_DICT) */
	int gosuu;		/* 辞書の語数 */
	int localf;
	int hlocalf;
};

typedef struct wnn_dic_info WNN_DIC_INFO;

/* Hideyuki Kishiba (Dec. 1, 1994)
   付属語ファイル情報受取用構造体 */
struct wnn_fzk_info {
        w_char	comment[WNN_COMMENT_LEN];	/* 付属語のコメント */
	char	fname[WNN_F_NAMELEN];		/* 付属語のファイル名 */
};

typedef struct wnn_fzk_info WNN_FZK_INFO;

struct wnn_file_stat {
    int type;			/* ファイルの種類
		WNN_STATIC_DICT		固定形式辞書
		WNN_UD_DICT		登録可能形式辞書
		WNN_HINDO_FILE		頻度ファイル
		WNN_FUZOKUGO_FILE	付属語ファイル
		WNN_NOT_A_JISHO		その他   */
};

typedef struct wnn_file_stat WNN_FILE_STAT;

/*	*/
extern	int	wnn_errorno;		/* Wnnのエラーはこの変数に報告される */

extern char *wnn_dic_types[];	/* "固定","登録","逆変換","正規" */

extern char *cwnn_dic_types[];	/* "固定","登録","逆変換","正規" */
extern char *bwnn_dic_types[];	/* "固定","登録","逆変換","正規" */

#define FILE_ALREADY_READ -2

/* この構造体は、ライブラリ内部で用いられる */
struct wnn_jserver_id {
	int	sd;
	char	js_name[40];
	int	js_dead;
	jmp_buf js_dead_env;	/* サーバが死んだ時に飛んでいくenv */
	int	js_dead_env_flg; /* jd_server_dead_envが有効か否か  */
};

typedef struct wnn_jserver_id WNN_JSERVER_ID;

/* この構造体は、ライブラリ内部で用いられる */
struct wnn_env {
	int		env_id;
	WNN_JSERVER_ID	*js_id;
	char		lang[16];	/* for exsample "ja_JP" */
	int		muhenkan_mode;
	int		bunsetsugiri_mode;
	int		kutouten_mode;
	int		kakko_mode;
	int		kigou_mode;
	int             autotune;
	int		autosave;
};

typedef struct wnn_env WNN_ENV;

struct wnn_param {
	int	n;	/* Ｎ(大)文節解析のＮ */
	int	nsho;	/* 大文節中の小文節の最大数 */
	int 	p1;	/* 自立語の頻度のパラメータ */
	int 	p2;	/* 小文節長のパラメータ */
	int 	p3;	/* 自立語長のパラメータ */
	int 	p4;	/* 今使ったよビットのパラメータ */
	int 	p5;	/* 辞書のパラメータ */
	int	p6;	/* 小文節の評価値のパラメータ */
	int	p7;	/* 大文節長のパラメータ */
	int	p8;	/* 小文節数のパラメータ */
	int 	p9;	/* 疑似品詞 数字の頻度 */
	int 	p10;	/* 疑似品詞 カナの頻度 *//* CWNN:英数の頻度 */
	int 	p11;	/* 疑似品詞 英数の頻度 *//* CWNN:記号の頻度 */
	int 	p12;	/* 疑似品詞 記号の頻度 *//* CWNN:開括弧の頻度 */
	int 	p13;	/* 疑似品詞 閉括弧の頻度 *//* CWNN:閉括弧の頻度 */
	int 	p14;	/* 疑似品詞 付属語の頻度 *//* BWNN:No of koho */
	int 	p15;	/* 疑似品詞 開括弧の頻度 *//* CWNN:Not used */
};

#define WNN_ENV_LAST_IS_FIRST_MASK      (1<<0)
#define WNN_ENV_COMPLEX_CONV_MASK       (1<<1)
#define WNN_ENV_OKURI_LEARN_MASK        (1<<2)
#define WNN_ENV_OKURI_MASK              (1<<3)
#define WNN_ENV_PREFIX_LEARN_MASK       (1<<4)
#define WNN_ENV_PREFIX_MASK             (1<<5)
#define WNN_ENV_SUFFIX_LEARN_MASK       (1<<6)
#define WNN_ENV_COMMON_LAERN_MASK       (1<<7)
#define WNN_ENV_FREQ_FUNC_MASK          (1<<8)
#define WNN_ENV_NUMERIC_MASK            (1<<9)
#define WNN_ENV_ALPHABET_MASK           (1<<10)
#define WNN_ENV_SYMBOL_MASK             (1<<11)
#define WNN_ENV_YURAGI_MASK		(1<<12)
#define WNN_ENV_RENDAKU_MASK		(1<<13)
#define WNN_ENV_BUNSETSUGIRI_LEARN_MASK (1<<14)
#define WNN_ENV_MUHENKAN_LEARN_MASK     (1<<15)
#define WNN_ENV_FI_RELATION_LEARN_MASK	(1<<16)
#define WNN_ENV_FI_FREQ_FUNC_MASK	(1<<17)
#define WNN_ENV_KUTOUTEN_MASK		(1<<18)
#define WNN_ENV_KAKKO_MASK		(1<<19)
#define WNN_ENV_KIGOU_MASK		(1<<20)

struct wnn_henkan_env {
        int     last_is_first_flag;     /* 最終使用最優先 */
        int     complex_flag;           /* 複合語優先 */
        int     okuri_learn_flag;       /* 送り基準学習 */
        int     okuri_flag;             /* 送り基準処理 */
        int     prefix_learn_flag;      /* 接頭語学習 */
        int     prefix_flag;            /* 接頭語候補 */
        int     suffix_learn_flag;      /* 接尾語学習 */
        int     common_learn_flag;      /* 汎用語学習 */
        int     freq_func_flag;         /* 頻度上昇確率関数 */
        int     numeric_flag;           /* 疑似数字の初期表示方法 */
        int     alphabet_flag;          /* 疑似アルファベットの初期表示方法 */
        int     symbol_flag;            /* 疑似記号の初期表示方法 */
	/*
	 * FI-Wnn 追加分 
	 * WATANABE Yasuhisa (Oct. 14, 1994)
	 */
	int	yuragi_flag;		/* 長音・ゆらぎ処理 */

	int	rendaku_flag;		/* 連濁処理 */
        int     bunsetsugiri_flag;      /* 文節切り学習 */
        int     muhenkan_flag;          /* 無変換学習 */
	int	fi_relation_learn_flag;	/* ＦＩ関係学習 */
	int	fi_freq_func_flag;	/* ＦＩ関係頻度上昇確率関数 */
	/*
	 * FI-Wnn 追加分
	 * Seiji KUWARI (Nov. 28, 1994)
	 */
	int	kutouten_flag;		/* 句読点 */
	int	kakko_flag;		/* 括弧 */
	int	kigou_flag;		/* 記号 */
};

/* dictionary type bit mask for js_fi_dic_list[_all] */
#define WNN_DIC_WNN_SYSTEM_MASK	(1<<0)
#define WNN_DIC_WNN_USER_MASK	(1<<1)
#define WNN_DIC_FI_SYSTEM_MASK	(1<<2)
#define WNN_DIC_FI_USER_MASK	(1<<3)
#define WNN_DIC_BUNSETSU_MASK  	(1<<4)
#define WNN_DIC_MUHENKAN_MASK 	(1<<5)
#define WNN_DIC_TEMPORARY_MASK  (1<<6)
#define WNN_DIC_NO_AUTOLEARN_MASK \
    (WNN_DIC_WNN_SYSTEM_MASK | WNN_DIC_WNN_USER_MASK)
#define WNN_DIC_AUTOLEARN_MASK \
    (WNN_DIC_BUNSETSU_MASK | WNN_DIC_MUHENKAN_MASK | WNN_DIC_TEMPORARY_MASK)
#define WNN_DIC_WNN_ALL_MASK \
    (WNN_DIC_NO_AUTOLEARN_MASK | WNN_DIC_AUTOLEARN_MASK)
#define WNN_DIC_FI_ALL_MASK \
    (WNN_DIC_FI_SYSTEM_MASK | WNN_DIC_FI_USER_MASK)
#define WNN_DIC_ALL_MASK \
    (WNN_DIC_WNN_ALL_MASK | WNN_DIC_FI_ALL_MASK)


struct wnn_file_info_struct {
	int	fid;		/* ファイル番号 */
	char	name[WNN_F_NAMELEN]; /* ファイル名 */
	int	localf;		/* サーバ・サイトのファイルかどうか
				   1: サーバ・サイト
				   0: クライアント・サイト   */
	int	type;		/* ファイルの種類 */
	int	ref_count;	/* (環境からの)参照数 */
};

typedef struct wnn_file_info_struct WNN_FILE_INFO_STRUCT;

#define WNN_VECT_L	((256+8*4-1)/(8*4) + 5)	/***** !!!!! ****/

struct	wnn_sho_bunsetsu {
	int	end;		/* 候補文節の end char index */
	int	start;		/* 候補文節の top char index */
	int	jiriend;	/* 候補文節の自立語 end char index */
	int	dic_no;		/* 自立語の辞書内のエントリ番号 */
	int	entry;		/* 候補文節の自立語辞書 entry */

	int	hinsi;		/* 自立語品詞 */
	int     status;		/* 大文節の先頭か */
	int	status_bkwd;	/* usiro の文節に接続できるか */
	int 	hindo;		/* 候補自立語の頻度 */ 
	int 	ima;		/* 候補自立語の今使ったよビット */ 
	int	kangovect;	/* 接続ベクトルテーブルへのポインタ */
	int 	hyoka;		/* 小文節評価値 */
	w_char	*kanji;		/* 自立語文字列 */
	w_char	*yomi;		/* 自立語の読み文字列 */
	w_char	*fuzoku;	/* 付属語文字列 */
/*
 *頻度については、頻度ファイルが指定されている時は、
 *hindo = 頻度ファイルの(実)頻度値 + 辞書内の(実)頻度値
 *ima = 頻度ファイルの今使ったよビット
 *
 *頻度ファイルが指定されていない時には、
 *hindo = 辞書内の(実)頻度値、ima = 辞書内の今使ったよビット
 *である。ここで、実頻度値とは、計算機内で7ビットに圧縮された値である。
 *仮想頻度値ではなく実頻度値を返すのは、変換結果のデバッグのためである。
 */
};

struct	wnn_dai_bunsetsu {
	int	end;		/* 候補文節の end char index */
	int	start;		/* 候補文節の top char index */
	struct	wnn_sho_bunsetsu	*sbn;	/* 小文節解析結果へのポインタ */
	int 	hyoka;		/* 大文節評価値 */
	int	sbncnt;		/* 小文節数 (次候補の場合は、次候補数)
				   DSD_SBNは、*sbn から sbncnt だけある */
};

struct	wnn_jdata {
	int	dic_no;		/* 辞書番号 */
	int	serial;		/* 辞書内のエントリ番号 */
	int	hinshi;		/* 品詞番号(品詞番号と品詞名の対応は、manual/etc
				   の下を参照) */
	int	hindo;		/* 頻度 */
	int	ima;		/* 今使ったよビット */
	int	int_hindo;	/* 辞書内頻度 */
	int	int_ima;	/* 辞書内、今使ったよビット */
	w_char   *yomi;		/* 読みの文字列 */
	w_char	*kanji;		/* 漢字文字列 */
	w_char	*com;		/* エントリのコメント文字列 */
/*
 *頻度については、頻度ファイルが指定されている時は、
 *hindo = 頻度ファイルの(仮想)頻度値、ima = 頻度ファイルの今使ったよビット
 *int_hindo = 辞書内の(仮想)頻度値、int_ima = 辞書内の今使ったよビット
 *
 *頻度ファイルが指定されていない時には、
 *hindo = 辞書内の(仮想)頻度値、ima = 辞書内の今使ったよビット
 *int_hindo = -1、int_ima = -1
 *
 *ただし、どちらの場合でも、エントリが使用中止の状態の時には、
 *hindo = -1, ima = 0 あるいは、
 *int_hindo = -1, int_ima = 0 となる。
 *ここで、(仮想)頻度値とは、計算機内で7ビットに圧縮された頻度値から、
 *実際の値を想定した値である。
 */
};

struct	wnn_ret_buf {
	int	size;		/* buf から alloc されている大きさ */
	char	*buf;		/* malloc などでとられた領域 */
};

/*
 * Hideyuki Kishiba (Sep. 20, 1994)
 * 使用ＦＩ関係代入用構造体を定義する
 */
struct  fi_rel_data {
    int		fi_dic_no;	/* ＦＩ関係辞書の辞書番号 */
    int		dic_no;		/* 接続定義辞書の辞書番号 */
    int		entry;		/* 接続定義候補のエントリ番号 */
    int		offset;		/* ＦＩ関係データのオフセット番号 */
    int		fi_hindo;	/* ＦＩ関係接続頻度の設定値 */
    int		fi_ima;		/* ＦＩ関係接続今ビットの設定値 */
};

typedef struct fi_rel_data FI_PRIO_DATA;

/*
 * Hideyuki Kishiba (Sep. 20, 1994)
 * 使用ＦＩ関係送受信用構造体を定義する
 */
struct wnn_fi_rel_buf {
    int		       size;	 /* alloc されているＦＩ関係データ数 */
    int		       num;	 /* 有効ＦＩ関係データ数 */
    struct fi_rel_data *fi_buf;  /* ＦＩ関係データ構造体配列へのポインタ */
};

/*
 * Hideyuki Kishiba (Nov. 24, 1994)
 * 前確定ＦＩ変換用構造体を定義する
 */
#define WNN_PREV_BUN_SUU 2  /* 覚えておく直前確定文節数 */

/* 直前に確定した文節の各情報を覚えておく構造体 */
struct wnn_prev_bun {
    int         dic_no;                 /* Wnn 辞書番号 */
    int         entry;                  /* エントリ番号 */
    int         real_kanjilen;          /* 候補文字数（付属語なし）*/
    w_char      kouho[256];    		/* 候補文字列（付属語あり）
					   256 == LENGTHKANJI */
    int         jirilen;                /* 自立語読み長 */
    int         hinsi;                  /* 品詞 */
};


#define	WNN_F_UNIQ_LEN	(sizeof(struct wnn_file_uniq))

/* hinsi name used in jlib and jserver */
#define WNN_HINSI_MEISI		"名詞"
#define WNN_HINSI_TANKAN      	"単漢字"
#define WNN_HINSI_SUUSI		"数詞"
#define WNN_HINSI_SETTOUO	"接頭語(お)"
#define WNN_HINSI_SETSUBI	"接尾語"
#define WNN_HINSI_RENDAKU	"連濁"
#define WNN_HINSI_TELNO		"電話番号"
#define WNN_HINSI_ZIPCODE	"郵便番号"
#define WNN_ROOT_HINSI_NODE	"/"
#define WNN_UNUSE_HINSI		9


/* この構造体は、ライブラリ内部で用いられる */
struct wnn_file_head {
  struct wnn_file_uniq{
    int time;
    int dev;
    int inode;
    char createhost[WNN_HOSTLEN];
  } file_uniq;
  struct wnn_file_uniq file_uniq_org;
  int file_type;
  char file_passwd[WNN_PASSWD_LEN];
};


#define WNN_HINSI_NAME_LEN 64

#define	WNN_FILE_HEADER_LEN	(WNN_PASSWD_LEN + 8 + WNN_FILE_STRING_LEN + \
				 sizeof(struct wnn_file_uniq) * 2 + 4)
				/*
				  8 is for future use 
				  4 is for file_type.
				 */

/*
  JSLIB function declaration
*/
#define	js_open(server, timeout)	js_open_lang(server, "ja_JP", timeout)
#define	js_connect(server,env_name)	js_connect_lang(server, env_name, "ja_JP")

extern	WNN_JSERVER_ID *js_open_lang();
extern	unsigned int	dispname_to_ipaddr();
extern	int		js_close();
extern	WNN_JSERVER_ID *js_change_current_jserver();
extern	struct wnn_env *js_connect_lang();
extern	int		js_disconnect();
extern	int		js_env_list();
extern	int		js_param_set();
extern	int		js_param_get();
extern	char	       *js_get_lang();
/**************************************/
extern int js_access();
extern int js_dic_add();
extern int js_dic_delete();
extern int js_dic_file_create();
extern int js_dic_file_create_client();
extern int js_dic_info();
extern int js_dic_list();
extern int js_dic_list_all();
extern int js_dic_use();
extern int js_env_exist();
extern int js_env_sticky();
extern int js_env_un_sticky();
extern int js_file_comment_set();
extern int js_file_discard();
extern int js_file_info();
extern int js_file_list();
extern int js_file_list_all();
extern int js_file_loaded();
extern int js_file_loaded_local();
extern int js_file_password_set();
extern int js_file_read();
extern int js_file_receive();
extern int js_file_remove();
extern int js_file_remove_client();
extern int js_file_send();
extern int js_file_stat();
extern int js_file_write();
extern void js_flush();
extern int js_fuzokugo_get();
extern int js_fuzokugo_set();
extern int js_hindo_file_create();
extern int js_hindo_file_create_client();
extern int js_hindo_set();
extern int js_hinsi_dicts();
extern int js_hinsi_list();
extern int js_hinsi_name();
extern int js_hinsi_number();
extern int js_hinsi_table_set();
extern int js_isconnect();
extern int js_kanren();
extern int js_kantan_dai();
extern int js_kantan_sho();
extern int js_kanzen_dai();
extern int js_kanzen_sho();
extern int js_kill();
extern int js_mkdir();
extern int js_version();
extern int js_who();
extern int js_word_add();
extern int js_word_comment_set();
extern int js_word_delete();
extern int js_word_info();
extern int js_word_search();
extern int js_word_search_by_env();


extern int js_access_add_host();
extern int js_access_remove_host();
extern int js_access_add_user();
extern int js_access_remove_user();
extern int js_access_enable();
extern int js_access_disable();
extern char **js_access_get_info();

extern int js_is_loaded_temporary_dic();
extern int js_temporary_dic_add();
extern int js_temporary_dic_delete();
extern int js_autolearning_word_add();
extern int js_temporary_word_add();

extern int js_set_autolearning_dic();
extern int js_get_autolearning_dic();

extern int js_set_henkan_env();
extern int js_get_henkan_env();
extern int js_get_henkan_env_local();
extern int js_set_henkan_hinsi();
extern int js_get_henkan_hinsi();
extern int js_henkan_with_data();


/*
 * Hideyuki Kishiba (Jul. 8, 1994)
 * New js_lib functions for FI-Wnn
 */
extern int js_fi_dic_add();
extern int js_fi_hindo_file_create();
extern int js_fi_hindo_file_create_client();

extern int js_fi_kanren();
extern int js_set_fi_priority();
extern int js_optimize_fi();

extern int js_fi_dic_list();
extern int js_fi_dic_list_all();
extern int js_fuzokugo_list();


#endif	/* _JSLIB_H_ */
