/*
 * $Id: rk_spclval.h,v 2.3.2.1 2000/08/04 05:37:16 kaneda Exp $
 */
/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/
/***********************************************************************
			rk_spclval.h
						87.12. 3  改 正

	本変換で使っている特殊な値の定義（外部にも出す）。マクロ
	関数も、ctype.hを使うもの以外はこちらで定義されている。
	rk_macros.h にincludeされている。
***********************************************************************/
/*	Version 3.0
 */
#ifndef _RK_SPCLVAL_H_
#define _RK_SPCLVAL_H_

#ifndef EOLTTR

typedef	unsigned int	letter; /* 文字は一般にこの型で表す */
typedef	unsigned char	uns_chr;

#define LTRHUG	(~(0xff000000))
	/* 内部表現 及び偽文字を除く、letterの取り得る最大値 */


	/* 以下に定義する値は、最上位バイトが255でなければならない。
	   これらのうち、外に出るのは EOLTTR、CHMSIG、NISEBP、LTREOFと
	   REDRAW（偽デリートとしてNISEDLを使う場合はNISEDLも）。*/

#define	EOLTTR	((letter)-1) /* 文字列の終端を表すコード */
			/* キー入力があったら必ず何かコードを返す状態にして
			   おくと、特に返すべきものがない時はこれを返す。*/
#define	ERRCOD	((letter)-2) /* (error)を表す内部コード */
#define	CHMSIG	((letter)-3) /* モードチェンジを表すコード */
#define	NISEBP	((letter)-4) /* エラー警告用の偽BELコード */
#define	VARRNG	((letter)-5) /* 変域が二つの文字の間であるのを示す内部コード*/
#define	UNUSDC	((letter)-6) /* マッチを失敗させるための内部コード */
#define	REASIG	((letter)-7) /* 表の再設定を要求するための内部コード */
#define	URBFCL	((letter)-8) /* 裏バッファの明示的クリアを要求する内部コード*/
#define	LTREOF  ((letter)-9) /* romkan_next()、同getc()がEOFの代わりに返すもの
				（できればEOFと同じにしたいが…）*/
#define	REDRAW	((letter)-10)/* Wnnに変換行のredrawを要求する特殊コード */
#define	NISEDL	((letter)-11)/* 偽デリートとして定数を使いたい人のために準備
				された定数（但し、使いたくなければ使わなくても
				いい）。変数nisedlにセットして使う */


	/* romkan_init3()の引数のフラグに使う値 */

#define RK_CHMOUT 01	/* モードチェンジを知らせるコードを返すか？ */
#define RK_KEYACK 02	/* キーインに対し必ず何かを返すか */
#define RK_DSPNIL 04	/* モード表示文字列無指定の時に空文字列を返すか（デフ
			   ォルトはNULL）。互換性保持のため */
#define RK_NONISE 010	/* 偽コードを出さないようにするか */
#define RK_REDRAW 020	/* Wnn用特殊フラグ（redraw用のフラグを出すかどうか）*/
#define RK_SIMPLD 040	/* deleteの動作を単純にするか */
#define RK_VERBOS 0100	/* 使用する表の一覧をリポートするか */


	/* コードの区別に使うマクロ */

#define	HG1BIT	(0x80000000) /* 最上位ビットだよ */
#define	SHUBET(X) ((letter)(X) >> (int)24)
			      /* 内部表現で、上１バイトを種別表現に使ってる */
#define	LWRMSK(X) ((X) & ~(0xff000000)) /* 上１バイトを取り除く */
#define	LWRCUT(X) ((X) &= ~(0xff000000)) /* 上１バイトを取り除く */

#define	is_HON(X) (SHUBET(X) == 0) /* 本物の文字か */
#define	NAIBEX(X) (0 < SHUBET(X) && SHUBET(X) < 0x80) /* 内部表現を示す値か */
#define	isNISE(X) (SHUBET(X) == 0x80) /* 偽物の文字か（最上位ビットが立つ） */
#define	isSPCL(X) (SHUBET(X) == 0xff) /* rk_spclval.hで定義される値かどうか*/
 /* NISEDLなどを含めた偽の文字であるかどうかを判定するには、~is_HON(X) か、
    isNISE(X) || isSPCL(X) として判定しないといけない。*/

#define toNISE(X) ((X) | HG1BIT)
#define to_HON(X) ((X) & ~HG1BIT)

 /* 互換性のため用意してある別名 */
#define REALCD(X) is_HON(X)
#define NISECD(X) isNISE(X)
#define SPCLCD(X) isSPCL(X)


	 /** 半角文字のコードのdefine */

#ifndef HNKAK1
#  ifdef IKIS
#    define HNKAK1 0xA8
#  else
#    define HNKAK1 0x8E
#  endif
#endif

	/** rk_bltinfn.c の補完のためのマクロ
	    （引数を複数回評価するものも多いので注意）*/

#define	HKKBGN	(HNKAK1 * 0x100 + 0xA1) /* 半角カナの句点 */
#define	HKKEND	(HNKAK1 * 0x100 + 0xDF) /*     〃    半濁点 */
#define	HIRBGN	(0xA4A1) /* ぁ */
#define	HIREND	(0xA4F3) /* ん */  /* ひらがな："ぁ"〜"ん" */
#define	KATBGN	(0xA5A1) /* ァ */
#define	KATEND	(0xA5F6) /* ヶ */  /* カタカナ："ァ"〜"ン"〜"ヶ" */

#define _to_kata(l) ((l) + (KATBGN - HIRBGN)) /** カタカナへ（定義域制限）*/
#define _to_hira(l) ((l) - (KATBGN - HIRBGN)) /** ひらがなへ（定義域制限）*/
#define	is_hira(l) (HIRBGN <= (l) && (l) <= HIREND) /** ひらがなか？ */
#define is_kata(l) (KATBGN <= (l) && (l) <= KATEND) /** カタカナか？ */
#define	is_kata2(l) (_to_kata(HIRBGN) <= (l) && (l) <= _to_kata(HIREND))
				/** 対応するひらがなのあるカタカナか？ */
#define to_kata(l) (is_hira(l) ? _to_kata(l) : (l)) /** カタカナへ */
#define to_hira(l) (is_kata2(l) ? _to_hira(l) : (l))
			/** ひらがなへ。「ヴヵヶ」はカタカナのまま残る。*/
#define is_hankata(l) (HKKBGN <= (l) && (l) <= HKKEND)
				/** 半角カナ（句点などを含む）か？ */



	/* その他のマクロ関数群（引数を複数回評価するものも多いので注意）*/

#define numberof(array) (sizeof(array) / sizeof(*array))

 /* ポインタをletterの列の末尾へもっていく。letter *lp */
#define	totail(lp) {while(*(lp) != EOLTTR) (lp)++;}

 /* 文字列へのポインタをその文字列の最後尾へ。totailのchar版。char *sp */
#define	strtail(sp) {while(*(sp)) (sp)++;}

	/* 限定版romkan_init3 */
#define romkan_init4(pathname, delchr, nisedl, keyin, bytecount, flg) \
	romkan_init3(pathname, delchr, nisedl, EOLTTR, \
		     keyin, bytecount, bytecount, 0, flg)

#define romkan_init5(pathname, delchr, keyin, bytecount, flg) \
	romkan_init4(pathname, delchr, toNISE(delchr), keyin, bytecount, flg)

        /* romkan_init3 for Multi-threaded */
#define romkan_init4_mt(pathname, delchr, nisedl, keyin, bytecount, flg) \
        romkan_init3_mt(pathname, delchr, nisedl, EOLTTR, \
			keyin, bytecount, bytecount, 0, flg)

#define romkan_init5_mt(pathname, delchr, keyin, bytecount, flg) \
        romkan_init4_mt(pathname, delchr, toNISE(delchr), keyin, bytecount, flg)

#endif	/* of ifndef EOLTTR */

#endif /* _RK_SPCLVAL_H_ */
