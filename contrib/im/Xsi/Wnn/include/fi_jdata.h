/*
 * $Id: fi_jdata.h,v 2.8.2.1 2000/08/04 05:37:13 kaneda Exp $
 */
/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/


#ifndef _FI_JDATA_H_
#define _FI_JDATA_H_

/*
 *                   fi_jdata.h
 *
 * Description:
 *     FI dictionary and FI hindo format
 */

/* FI index table size */
#define MAX_FI_INDEX_TABLE 256

/*
 * FI dictionary data format
 */
struct fi_dic_data {
    unsigned int  dic_entry;	/* jishono and entry */
    unsigned char jirilen;	/* 自立語長 */
    unsigned char ppp_id;	/* 助詞ＩＤ */
    unsigned char hindo;	/* 接続関係頻度 */
    unsigned char bitdata;	/* ビットデータ部 */
};

/*
 * FI hindo data format
 */
struct fi_hindo_data {
    unsigned char hindo;	/* 接続関係頻度 */
    unsigned char bitdata;	/* ビットデータ部 */
};

/*
 * secondary index table for FI dictionary
 */
struct fi_second {
    unsigned char      *entry;	  /* エントリ番号 */
    union fi_data {
	struct fi_dic_data **dic; /* FI dictionary data ポインタ配列 */
	struct fi_hindo_data **hindo; /*FI hindo data ポインタ配列 */
    } fi_data;
};

/*
 * primary index table for FI dictionary
 */
struct fi_primary {
    int			primary;	/* primary table entry 数 */
    unsigned char 	*secondary;	/* secondary table entry 数の配列 */
    struct fi_second 	**stable;	/* secondary table へのポインタ */
};

/*
 * FI dictionary memory format
 */  
struct FI_JT {
  char hpasswd[WNN_PASSWD_LEN];	/* 頻度部分のパスワード */
  int syurui;			/* ＦＩ関係辞書タイプ (system or user) */

  int maxcomment;		/* コメント文字数 */
  w_char *comment;		/* コメント文字列 */

  short dirty;			/* 辞書データ変更フラグ */

  unsigned char maxjisho;	/* 接続定義Ｗｎｎ辞書数 */
  struct wnn_file_uniq *jisho_uniq;
                                /* 接続定義Ｗｎｎ辞書 f_uniq_org 配列 */
  struct fi_primary **ptable;	/* 各 primary index table へのポインタ */
};

/*
 * FI hindo memory format
 */  
struct FI_HJT {
  struct wnn_file_uniq fi_dic_uniq;	/* 対応ＦＩ関係辞書 f_uniq_org */

  int maxcomment;		/* コメント文字数 */
  w_char *comment;		/* コメント文字列 */

  unsigned char maxjisho;	/* 接続定義Ｗｎｎ辞書数 */
  short hdirty;			/* 頻度データ変更フラグ */

  struct fi_primary **ptable;	/* 各 primary index table へのポインタ */
};


typedef struct {
    struct fi_primary    *dic_1i;     /* fi dic primary   index table pointer   */ 
    struct fi_second     *dic_2i;     /* fi dic secondary index table pointer   */ 
    struct fi_primary    *hin_1i;     /* fi hindo primary   index table pointer */ 
    struct fi_second     *hin_2i;     /* fi hindo secondary index table pointer */ 
    int                   bb;         /* binary search breaked point            */
} FI_DICD_FOR_GROW_UP;


/*
 * FI set priority structure
 */
typedef struct {
    int		fi_dicid;	/* FI internal relation dic ID */
    int		entry;		/* FI entry for B */
    int		offset;		/* FI relation data offset */
    int		imaop;		/* FI ima-bit operation flag */
    int		hinop;		/* FI hindo operation flag */
} FI_SET_PRIORITY;

/*
 * FI write fi data structure
 */
typedef struct
{
    int		dic_id;		/* その文節の使用されたWnn辞書ID */
    int		entry_id;       /* その文節のWnn辞書内でのエントリーID */
    int		fisysdic;       /* Wnn辞書IDに対応したFIsys辞書関係定義領域番号 */
    int		fiusrdic;       /* Wnn辞書IDに対応したFIusr辞書関係定義領域番号 */
} FII_BUN_ST;

typedef struct
{
    short	mode;		/* FI関係モード */
    short	auto_reverse;	/* 自動逆転FI関係 flag */
    short	RERU_bit;	/* れる（られる） bit */
    short	SERU_bit;	/* せる（させる） bit */
    FII_BUN_ST 	a;     		/* 文節 Aの identifier */
    unsigned char a_yomi_len;	/* 文節 Aの 読み自立語長 */
    FII_BUN_ST  b; 		/* 文節 Bの identifier */
    unsigned char b_yomi_len;	/* 文節 Bの 読み自立語長 */
} FII_DIC_DATA;
    
#endif /* _FI_JDATA_H_ */
