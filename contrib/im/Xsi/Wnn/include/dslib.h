/*
 * $Id: dslib.h,v 2.15.2.1 2000/08/04 05:37:12 kaneda Exp $
 */

/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/

/*	Version 4.0
 */
/*
	Modified	Feb.14,1993		tna@kyoto-sr.co.jp
			Feb.15,1993		tna@kyoto-sr.co.jp
*/
/*
	Nihongo	Henkan	Library Header File
*/
#ifndef _DSLIB_H_
#define _DSLIB_H_

#ifndef	_WNN_SETJMP
#define	_WNN_SETJMP
#include <setjmp.h>
#endif

#ifndef	w_char
#define	w_char	unsigned short
#endif	/* w_char */

#include "jslib.h"

/*	Wnn constant
*/
#define WNN_IPADDRLEN 40
#define DS_S_BUF_SIZ		9216    /* NEVER change this */
#define DS_R_BUF_SIZ		9216    /* NEVER change this */
#define DM_S_BUF_SIZ      	256
#define DM_R_BUF_SIZ	  	256

#define JJ_MAX			3

#define JS_FILES_ALLOC_SIZE	64
#define DS_FILES_ALLOC_SIZE	128

struct serv_addr {
        char	addr[WNN_IPADDRLEN];
        int	addrlen;
        int	addrtype;
	int	portno;
	int	jserver;
};

typedef struct serv_addr	SERV_ADDR;

struct wnn_dserver_id {
	int	sd;
	SERV_ADDR	ad;
	int	ds_dead;
	jmp_buf ds_dead_env;	/* サーバが死んだ時に飛んでいくenv */
	int	ds_dead_env_flg; /* dd_server_dead_envが有効か否か  */
	int	rbc;
	int	rbp;
	unsigned char rcv_buf[DS_R_BUF_SIZ];
};

typedef struct wnn_dserver_id WNN_DSERVER_ID;

#define MAX_WNNDS_NUM	64
#define MAX_SERVER_LIST 4

#define WNNDEFS "/wnndefs"
#define WNNDEFS_DATA 512


struct wnn_dic_file_info {
    int	file_type;
    char	passwd[ WNN_PASSWD_LEN ];
    char	hpasswd[ WNN_PASSWD_LEN ];
    struct	wnn_file_uniq funiq;
    int		maxserial;
    int		gosuu;
    int		maxcomment;
    w_char comment[WNN_COMMENT_LEN];
};

typedef struct wnn_dic_file_info	WNN_DIC_FILE_INFO;


struct wnn_hindo_info_struct {
    int	file_type;
    char	passwd[ WNN_PASSWD_LEN ];
    struct	wnn_file_uniq funiq;
};

typedef	struct wnn_hindo_info_struct	WNN_HINDO_INFO_STRUCT;

struct  ds_jdata {
    /* struct jdata <jdata.h> is modified for dslib */
  w_char *yomi,*kanji,*comment;
  short   which ;               /* gyaku henkan? */
  int   serial;                 /* index is a serial number of the first
                                 entry which is stored in this entry*/
  int   kosuu;                  /* this means the number of elements in this
                                 entry */
  struct  ds_jdata  *jptr; 	/* pointer to another jdata which
                                 points out a jdata of the same yomi
                                 but (always)different jishono*/
  unsigned short   *hinsi;
  unsigned char *hindo;                 /* 頻度 */
  unsigned char *hindo_in;              /* 内部頻度 */
};

struct ds_inspect_struct {
    w_char	*yomi;
/*    struct ds_jdata *jd;	D.K 5/4 */
    struct jdata	*jd;
};

struct ds_word_search_struct {
    int	   match_len;		/* maxmum matching length */
    int	   total;		/* total words : don't need? */
/*    struct ds_jdata **jd;	D.K  5/4 */
    struct jdata	**jd;
};


struct alloc_data {
    unsigned char	*data;
    int			alloc_max;
    struct alloc_data 	*next;
};

#define ALLOC_DATA_SIZE 1024

/*
 * Macros added by D.K
 *
 */

#define DS_FILE_UNLOAD		-2
#define DS_FILE_LOADED_JSERVER  -1
#define DS_FILE_LOADED_DEFAULT	0

#define	GET_WNN_DSERVER_ID(ds_id) \
                             (&dserver_id_table[(ds_id)])
#define	GET_WNN_DSERVER_ID_FID(fid) \
                             (&dserver_id_table[files[(fid)].ds_id])
#define	LOADED_ON_JSERVER_P(fid) \
                      (files[(fid)].ds_id == DS_FILE_LOADED_JSERVER)

#define handler_of_dserver_dead(err_val) \
{ \
    if (current_ds) { \
        if(current_ds->ds_dead || setjmp(current_dserver_dead)) { \
	    wnn_errorno=WNN_DS_DEAD; \
	    return err_val; \
        } \
        wnn_errorno = 0; /* here initialize wnn_errorno; */    \
    } \
}
#endif /* _DSLIB_H_ */
