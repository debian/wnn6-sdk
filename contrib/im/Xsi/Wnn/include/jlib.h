/*
 * $Id: jlib.h,v 2.6.2.1 2000/08/04 05:37:14 kaneda Exp $
 */
/*
WNN6 CLIENT LIBRARY--SOFTWARE LICENSE TERMS AND CONDITIONS


Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993

Preamble

These Wnn6 Client Library--Software License Terms and Conditions
 (the "License Agreement") shall state the conditions under which you are
 permitted to copy, distribute or modify the software which can be used
 to create Wnn6 Client Library (the "Wnn6 Client Library").  The License
 Agreement can be freely copied and distributed verbatim, however, you
 shall NOT add, delete or change anything on the License Agreement.

OMRON Corporation and OMRON Software Co., Ltd. (collectively referred to
 as "OMRON") jointly developed the Wnn6 Software (development code name
 is FI-Wnn), based on the Wnn Software.  Starting from November, 1st, 1998,
 OMRON publishes the source code of the Wnn6 Client Library, and OMRON
 permits anyone to copy, distribute or change the Wnn6 Client Library under
 the License Agreement.

Wnn6 Client Library is based on the original version of Wnn developed by
 Kyoto University Research Institute for Mathematical Sciences (KURIMS),
 OMRON Corporation and ASTEC Inc.

Article 1.  Definition.

"Source Code" means the embodiment of the computer code, readable and
 understandable by a programmer of ordinary skills.  It includes related
 source code level system documentation, comments and procedural code.

"Object File" means a file, in substantially binary form, which is directly
 executable by a computer after linking applicable files.

"Library" means a file, composed of several Object Files, which is directly
 executable by a computer after linking applicable files.

"Software" means a set of Source Code including information on its use.

"Wnn6 Client Library" the computer program, originally supplied by OMRON,
 which can be used to create Wnn6 Client Library.

"Executable Module" means a file, created after linking Object Files or
 Library, which is directly executable by a computer.

"User" means anyone who uses the Wnn6 Client Library under the License
 Agreement.

Article 2.  Copyright

2.1  OMRON Corporation and OMRON Software Co., Ltd. jointly own the Wnn6
 Client Library, including, without limitation, its copyright.

2.2  Following words followed by the above copyright notices appear
 in all supporting documentation of software based on Wnn6 Client Library:

  This software is based on the original version of Wnn6 Client Library
  developed by OMRON Corporation and OMRON Software Co., Ltd. and also based on
  the original version of Wnn developed by Kyoto University Research Institute
  for Mathematical Sciences (KURIMS), OMRON Corporation and ASTEC Inc.

Article 3.  Grant

3.1  A User is permitted to make and distribute verbatim copies of
 the Wnn6 Client Library, including verbatim of copies of the License
 Agreement, under the License Agreement.

3.2  A User is permitted to modify the Wnn6 Client Library to create
 Software ("Modified Software") under the License Agreement.  A User
 is also permitted to make or distribute copies of Modified Software,
 including verbatim copies of the License Agreement with the following
 information.  Upon modifying the Wnn6 Client Library, a User MUST insert
 comments--stating the name of the User, the reason for the modifications,
 the date of the modifications, additional terms and conditions on the
 part of the modifications if there is any, and potential risks of using
 the Modified Software if they are known--right after the end of the
 License Agreement (or the last comment, if comments are inserted already).

3.3  A User is permitted to create Library or Executable Modules by
 modifying the Wnn6 Client Library in whole or in part under the License
 Agreement.  A User is also permitted to make or distribute copies of
 Library or Executable Modules with verbatim copies of the License
 Agreement under the License Agreement.  Upon modifying the Wnn6 Client
 Library for creating Library or Executable Modules, except for porting
 a computer, a User MUST add a text file to a package of the Wnn6 Client
 Library, providing information on the name of the User, the reason for
 the modifications, the date of the modifications, additional terms and
 conditions on the part of the modifications if there is any, and potential
 risks associated with using the modified Wnn6 Client Library, Library or
 Executable Modules if they are known.

3.4  A User is permitted to incorporate the Wnn6 Client Library in whole
 or in part into another Software, although its license terms and
 conditions may be different from the License Agreement, if such
 incorporation or use associated with the incorporation does NOT violate
 the License Agreement.

Article 4. Warranty

THE WNN6 CLIENT LIBRARY IS PROVIDED BY OMRON ON AN "AS IS" BAISIS.
  OMRON EXPRESSLY DISLCIAMS ANY AND ALL WRRANTIES, EXPRESS OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, IN CONNECTION WITH THE WNN6 CLIENT LIBRARY
 OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT LIBRARY.  IN NO EVENT
 SHALL OMRON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE
 OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER IN CONNECTION WITH THE
 WNN6 CLIENT LIBRARY OR THE USE OR OTHER DEALING IN THE WNN6 CLIENT
LIBRARY.

***************************************************************************
Wnn6 Client Library :
(C) Copyright OMRON Corporation.       1995,1998,2000 all rights reserved.
(C) Copyright OMRON Software Co., Ltd. 1995,1998,2000 all rights reserved.

Wnn Software :
(C) Copyright Kyoto University Research Institute for Mathematical Sciences
     1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright OMRON Corporation. 1987, 1988, 1989, 1990, 1991, 1992, 1993
(C) Copyright ASCTEC, Inc.  1987, 1988, 1989, 1990, 1991, 1992, 1993
***************************************************************************

Comments on Modifications:
*/
/*	Version 3.0
 */
/*
	Nihongo	Henkan	Library Header File

	08/Jul/1987	(wnn_errorno)
	24/Apr/1987
	09/Mar/1987	(update server_dead)
	25/Feb/1987	(update wchar_t)
	18/Feb/1987	(update Comment)
	28/Nov/1986
	23/Oct/1986
*/
#ifndef _JLIB_H_
#define _JLIB_H_

#include <setjmp.h>

#ifndef w_char
#define w_char	unsigned short
#endif /* w_char */
#ifndef WCHAR_T
#define wchar_t	w_char
#define wchar	w_char
#define WCHAR_T
#if defined(SOLARIS) || defined(SGI)
#define _WCHAR_T
#endif 
#endif /* WCHAR_T */

#define JLIB

#define	S_BUF_SIZ	1024	/* NEVER change this */
#define	R_BUF_SIZ	1024	/* NEVER change this */

struct	kouho_entry {
	int	s_ichi;		/*	かなバッファへのオフセット	*/
	int	jl;		/*	自立語の長さ			*/
	int	fl;		/*	付属語の長さ			*/
	int	pl;		/*	ディスプレイ時の長さ		*/
	int	jishono;	/*	自立語の辞書番号		*/
	int	serial;		/*	自立語の番号			*/
	w_char	*k_data;	/*	漢字バッファへのポインタ	*/
};
#define	KOUHO_ENT	struct	kouho_entry


struct	jikouho_entry {
	int	jl;		/*	自立語の長さ			*/
	int	jishono;	/*	自立語の辞書番号		*/
	int	serial;		/*	自立語の番号			*/
	w_char	*k_data;	/*	漢字バッファへのポインタ	*/
};
#define	JIKOUHO_ENT	struct	jikouho_entry


struct	bunjoho {
	w_char	*kana_buf;	/*	かなバッファ			*/
	int	kana_size;	/*	かなバッファの長さ		*/
	KOUHO_ENT *klist;	/*	候補リスト・バッファ		*/
	int	klist_size;	/*	候補リスト・バッファの長さ	*/
	w_char	*kanji_buf;	/*	漢字バッファ			*/
	int	kanji_buf_size;	/*	漢字バッファ の長さ		*/
};


struct	jikouhojoho{
	JIKOUHO_ENT *jlist;	/*	次候補バッファ			*/
	int	jlist_size;	/*	次候補バッファの長さ		*/
	w_char	*kanji_buf;	/*	漢字バッファ			*/
	int	kanji_buf_size;	/*	漢字バッファの長さ		*/
};
#define	JIKOUHOJOHO		struct	jikouhojoho


struct	jishojoho{
	w_char	*k_data;	/*	漢字バッファへのポインタ	*/
	int	bumpo;		/*	文法情報			*/
	int	hindo;		/*	頻度				*/
	int	jisho;		/*	辞書番号			*/
	int	serial;		/*	項目番号			*/
};
#define	JISHOJOHO	struct	jishojoho


struct dicinfo{
	int	dic_no;		/*	辞書番号			*/
	int	dic_size;	/*	単語数				*/
	int	ttl_hindo;	/*	頻度総数			*/
	int	dic_type;	/*	1:ユーザ,2:システム		*/
	int	udp;		/*	0:使用中でない,1:使用中		*/
	int	prio;		/*	辞書プライオリティ		*/
	int	rdonly;		/*	0:ライト可能,1:リードオンリー	*/
	char	*file_name;	/*	辞書ファイル名			*/
	char	*h_file_name;	/*	頻度ファイル名			*/
};
#define	DICINFO	struct	dicinfo

extern	jmp_buf	jd_server_dead_env;	/* サーバが死んだ時に飛んでいくenv */
extern	int	jd_server_dead_env_flg; /* jd_server_dead_envが有効か否か  */
extern	int	wnn_errorno;		/* Wnnのエラーはこの変数に報告される */

/* header file for dic_syurui */
#define NO_EXIST -1
#define NOT_A_JISHO 0
#define USER_DIC 1
#define SYSTEM_DIC 3

struct jwho {
    int sd;   /* socket discripter in jserver */
    char *user_name;   /* user name */
    char *host_name;   /* host name */
};

#define JWHO struct jwho

#endif /* _JLIB_H_ */
/*
  Local Variables:
  kanji-flag: t
  End:
*/
